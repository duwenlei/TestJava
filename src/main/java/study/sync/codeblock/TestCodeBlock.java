package study.sync.codeblock;


public class TestCodeBlock {
    public static void main(String[] args) {
        SyncThread syncThread = new SyncThread();

        Thread thread1 = new Thread(syncThread,"thread1");
        Thread thread2 = new Thread(syncThread,"thread2");
        Thread thread3 = new Thread(syncThread,"thread3");
        Thread thread4 = new Thread(syncThread,"thread4");
        Thread thread5 = new Thread(syncThread,"thread5");
        Thread thread6 = new Thread(syncThread,"thread6");
        Thread thread7 = new Thread(syncThread,"thread7");
        Thread thread8 = new Thread(syncThread,"thread8");
        Thread thread9 = new Thread(syncThread,"thread9");

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();
        thread6.start();
        thread7.start();
        thread8.start();
        thread9.start();
    }
}
