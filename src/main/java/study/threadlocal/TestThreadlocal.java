package study.threadlocal;

public class TestThreadlocal {
    private static ThreadLocal<Integer> threadLocal = new ThreadLocal<>();

    public static void main(String[] args) {
        threadLocal.set(1);

        System.out.println(threadLocal.get());
        System.gc();
    }
}
