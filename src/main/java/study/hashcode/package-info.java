package study.hashcode;

/*
 * hashCode 是java.lang.Object 提供的方法，用来得到对象的散列值
 * 此方法是一个 native 方法，所以实现 hashCode 方法在 JVM 中，例如实用 hotspot jvm 则实现在 hotspot 中
 * hashCode 方法返回的不一定是一个对象的内存地址，有可能是自增，或者时根据内存地址在加上逻辑运算得出。也就是说 hashCode 的值有很多配置规则
 *
 * -XX:hashCode=n 这个配置为 JVM 在生成 hashCode 的计算配置。
 *
 * n==0 使用随机方式生成
 * n==1 使用内存地址加逻辑运算得出
 * n==2 所有返回都为1
 * n==3 使用自增序列赋值
 * n==4 内存地址
 * n==其他
 *
 * hashCode 只有在调用 hashCode() 方法时才进行计算。
 *
 * hashCode 相同时，equals 一定相同，equals 相同时 hashCode 不一定相同。
 *
 *
 */

/*
 * HotSpot JVM 计算方式
    static inline intptr_t get_next_hash(Thread * Self, oop obj) {
        intptr_t value = 0 ;
        if (hashCode == 0) {
            // This form uses an unguarded global Park-Miller RNG,
            // so it's possible for two threads to race and generate the same RNG.
            // On MP system we'll have lots of RW access to a global, so the
            // mechanism induces lots of coherency traffic.
            value = os::random() ;
        } else
        if (hashCode == 1) {
            // This variation has the property of being stable (idempotent)
            // between STW operations.  This can be useful in some of the 1-0
            // synchronization schemes.
            intptr_t addrBits = intptr_t(obj) >> 3 ;
            value = addrBits ^ (addrBits >> 5) ^ GVars.stwRandom ;
        } else
        if (hashCode == 2) {
            value = 1 ;            // for sensitivity testing
        } else
        if (hashCode == 3) {
            value = ++GVars.hcSequence ;
        } else
        if (hashCode == 4) {
            value = intptr_t(obj) ;
        } else {
            // Marsaglia's xor-shift scheme with thread-specific state
            // This is probably the best overall implementation -- we'll
            // likely make this the default in future releases.
            unsigned t = Self->_hashStateX ;
            t ^= (t << 11) ;
            Self->_hashStateX = Self->_hashStateY ;
            Self->_hashStateY = Self->_hashStateZ ;
            Self->_hashStateZ = Self->_hashStateW ;
            unsigned v = Self->_hashStateW ;
            v = (v ^ (v >> 19)) ^ (t ^ (t >> 8)) ;
            Self->_hashStateW = v ;
            value = v ;
        }

        value &= markOopDesc::hash_mask;
        if (value == 0) value = 0xBAD ;
        assert (value != markOopDesc::no_hash, "invariant") ;
        TEVENT (hashCode: GENERATE) ;
        return value;
    }
 */