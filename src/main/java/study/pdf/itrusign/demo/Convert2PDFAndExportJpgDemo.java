package study.pdf.itrusign.demo;

import study.pdf.itrusign.httpclient.HttpClientUtil;

import java.util.HashMap;
import java.util.Map;

public class Convert2PDFAndExportJpgDemo {

	public static void main(String[] args) {
		//目前文档转换功能服务，只提供了一个接口，
		//该接口实现了转换成PDF，同时导出JPG格式图片的功能
		Map<String, String> params = new HashMap<String, String>();
		//接口路径
		String doc_server_interface = "http://175.102.179.35:8080/doc.rpc";
		//待转换文档路径，在windows服务上面对应的路径是：z:/document/test/test.docx文件
		params.put("path", "/test/test.docx");
		//生成的test.pdf文件与test.docx在同一个路径下，导出的图片也创建一个同名的文件夹test下
		String result = HttpClientUtil.sendHttpPost(doc_server_interface, params);

		System.out.println(result);

	}

}
