package study.pdf;


import java.io.*;

import fr.opensagres.poi.xwpf.converter.core.IXWPFConverter;
import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

/**
 * @author Rocca
 */
public class ConverterToPdf {

    private static final IXWPFConverter pdfOptionsIXWPFConverter = PdfConverter.getInstance();

    public static void main(String[] args) throws Exception {
        for (int i = 0; i< 10;i++){
            long startTime = System.currentTimeMillis();
            String outpath = "C:\\Users\\duwenlei\\Documents\\docx\\" + System.currentTimeMillis() + ".pdf";
            OutputStream target = new FileOutputStream(outpath);
            XWPFDocument document = new XWPFDocument(new FileInputStream("C:\\Users\\duwenlei\\Documents\\docx\\2355.docx"));
            PdfOptions options = PdfOptions.create();
            pdfOptionsIXWPFConverter.convert(document, target, options);
            target.close();
            System.out.println(System.currentTimeMillis() - startTime+"/ms");
        }
    }
}
