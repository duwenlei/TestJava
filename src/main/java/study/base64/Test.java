package study.base64;

public class Test {

    public static void main(String[] args) {
        Student s = new Student();
        test(s);
        System.out.println(s.name);
        System.out.println(s.age);
    }

    public static void test(Student s) {
        s.name = "dwl";
        s.age = 19;
    }
}
