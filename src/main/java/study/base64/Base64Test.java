package study.base64;


import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/*
 * https://zh.wikipedia.org/wiki/Base64
 * */
public class Base64Test {
    private static final String base = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    public static void main(String[] args) {
        String data = "杜";
        byte[] bytes = data.getBytes(Charset.forName("utf-8"));
        StringBuilder sb = new StringBuilder();
        for (int b : bytes) {
            //int val = 0xff & b;
            sb.append(int2bytes(b));
        }

        List list = new ArrayList();
        int sbLen = sb.length();
        int index = 0;
        while (true) {
            if (index + 6 >= sbLen) {
                String lastBytes = sb.substring(index, sbLen);
                if (lastBytes.length() == 2) {
                    list.add(lastBytes.concat("0000"));
                } else if (lastBytes.length() == 4) {
                    list.add(lastBytes.concat("00"));
                }
                break;
            }
            list.add(sb.substring(index, index + 6));
            index = index + 6;
        }
        System.out.println(list.toString());
        StringBuilder base64Enc = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            String b = (String) list.get(i);
            base64Enc.append(getBaseWord(bytes2int(b)));
        }
        int padding = data.length() % 3;
        switch (padding){
            case 1:
                base64Enc.append("==");
                break;
            case 2:
                base64Enc.append("=");
                break;
        }

        System.out.println("base64 encode:" + base64Enc.toString());
    }

    public static String int2bytes(int num) {
        StringBuilder sb = new StringBuilder();
        int currentNum = num;

        while (currentNum != 0) {
            if (currentNum % 2 == 0) {
                sb.append(0);
            } else {
                sb.append(1);
            }
            currentNum = currentNum / 2;
        }
        while (sb.length() < 8) {
            sb.append(0);
        }
        return sb.reverse().toString();
    }

    public static int bytes2int(String b) {
        String[] bs = b.split("");
        int num = 0;
        int exponent = bs.length - 1;
        for (int i = 0; i < bs.length; i++) {
            int j = Integer.valueOf(bs[i]);
            num = (int) (Math.pow(2, exponent - i) * j) + num;
        }
        return num;
    }

    public static String getBaseWord(int index) {
        return base.split("")[index];
    }
}
