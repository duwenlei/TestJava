package study.twosum;

public class TwoSum {
    public static Integer[] getIndex(Integer[] arr, Integer target) {
        Integer[] result = new Integer[2];
        for (int i = 0; i < arr.length; i++) {
            for (int j = i; j < arr.length; j++) {
                if (arr[i] + arr[j] == target) {
                    result[0] = i;
                    result[1] = j;
                    break;
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Integer[] nums = {2, 7, 11, 15};
        int target = 9;
        long startTime = System.currentTimeMillis();
        Integer[] result = getIndex(nums, target);
        long endTime = System.currentTimeMillis();
        System.out.printf("[%d,%d]", result[0], result[1]);
        System.out.println("时间消耗：" + (endTime - startTime));
    }
}
