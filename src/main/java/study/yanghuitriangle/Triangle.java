package study.yanghuitriangle;

public class Triangle {
    public static void yhTriangle(int rowCount) {
        Integer[] tg = {1};
        for (int i = 1; i <= rowCount; i++) {

            //初始化并站位
            Integer[] tmp = initArray(i);

            if (i == 1) {
                arrToString(tg);
                System.out.println();
                continue;
            }
            tmp[0] = 1;
            tmp[i - 1] = 1;

            if (i == 2) {
                tg = tmp;
                arrToString(tg);
                System.out.println();
                continue;
            }
            for (int j = 1; j < i - 1; j++) {
                tmp[j] = tg[j - 1] + tg[j];
            }
            tg = tmp;
            arrToString(tg);
            System.out.println();
        }
    }

    public static void arrToString(Integer[] arr) {
        System.out.print("[ ");
        for (int i = 0 ;i< arr.length;i++){
            System.out.print(arr[i]);
            if (i != arr.length -1){
                System.out.print(",");
            }
        }
        System.out.print("]");
    }

    public static Integer[] initArray(int size) {
        Integer[] arr = new Integer[size];
        for (int i = 0; i < size; i++) {
            arr[i] = 1;
        }
        return arr;
    }

    public static void main(String[] args) {
        yhTriangle(20);
    }
}
