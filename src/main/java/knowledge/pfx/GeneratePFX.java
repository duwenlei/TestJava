package knowledge.pfx;

import org.apache.commons.codec.binary.Base64;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

public class GeneratePFX {
    /**
     * 1.转换Cert
     * 2.转换 private key
     * 3.生成 KeyStore
     * 4.将cert 和 private key 填入 keyStroe 中
     *
     * @param certB64
     * @param priKeyB64
     */
    public static void generate(String certB64, String priKeyB64) {
        try {
            byte[] certByte = Base64.decodeBase64(certB64);
            ByteArrayInputStream inputStream = new ByteArrayInputStream(certByte);

            CertificateFactory factory = CertificateFactory.getInstance("x.509");
            Certificate certificate = factory.generateCertificate(inputStream);

            // Key
            byte[] key = Base64.decodeBase64(priKeyB64);

            // Certificate[] chain
            Certificate[] certificates = new Certificate[1];
            certificates[0] = certificate;

            KeyStore keyStore = KeyStore.getInstance("jks");
            keyStore.load(null);
            keyStore.setKeyEntry("cert", key, certificates);
            keyStore.store(new FileOutputStream(new File("/home/duwenlei/keystore.pfx")), "password".toCharArray());

        } catch (CertificateException e) {
            System.out.println("证书转换失败");
            e.printStackTrace();
        } catch (KeyStoreException e) {
            System.out.println("生成 KeyStroe 失败");
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
