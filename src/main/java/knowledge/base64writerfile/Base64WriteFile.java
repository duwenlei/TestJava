package knowledge.base64writerfile;

import org.apache.commons.codec.binary.Base64;

import java.io.*;

public class Base64WriteFile {
    public static void main(String[] args) throws IOException {
        StringBuffer b64File = new StringBuffer("a");
        writeFile(b64File.toString());
        //readFile("1");
    }

    public static void writeFile(String b64File) throws IOException {
        byte[] pdfByte = Base64.decodeBase64(b64File);
        InputStream inputStream = new ByteArrayInputStream(pdfByte);
        FileOutputStream out = new FileOutputStream(new File("/home/duwenlei/Downloads/cnm111.pdf"));

        int len;
        byte[]  buffer = new byte[1024];

        while ((len = inputStream.read(buffer)) != -1){
            out.write(buffer,0,len);
        }
        out.close();
        inputStream.close();
    }

    public static void readFile(String target) throws IOException {
        FileInputStream inputStream = new FileInputStream("/home/duwenlei/Downloads/1.docx");
        ByteArrayOutputStream out =  new ByteArrayOutputStream();

        int len;
        byte[]  buffer = new byte[1024];

        while ((len = inputStream.read(buffer)) != -1){
            out.write(buffer,0,len);
        }

        inputStream.close();

        byte[] fileByte = out.toByteArray();
        out.close();

        System.out.println(Base64.encodeBase64String(fileByte));
    }
}
