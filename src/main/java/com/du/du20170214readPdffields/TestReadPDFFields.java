package com.du.du20170214readPdffields;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.*;

import java.awt.print.PageFormat;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * Created by duwenlei on 17-2-15.
 */
public class TestReadPDFFields {
    public static void main(String[] args) throws IOException, DocumentException {
//        PdfReader reader = new PdfReader("/home/duwenlei/Downloads/zqzrxy20170309001.pdf");
//        AcroFields fields = reader.getAcroFields();
//        Map mapFields = fields.getFields();
//
//        Set keySet = mapFields.keySet();
//        for (Object key : keySet) {
//            System.out.println("field name :" + key);
//        }
        test();
    }


    /**
     * 测试pdf页面大小
     *
     * */
    public static void test() throws IOException {
//        PdfReader reader = new PdfReader("/home/duwenlei/IdeaProjects/Drag/test/img/heng.pdf");
        PdfReader reader = new PdfReader("/home/duwenlei/IdeaProjects/espDemo/java/resources/esp/Go For C++ Programmers.pdf");
        Rectangle A3 = PageSize.A3;
        Rectangle A4 = PageSize.A4;
        Rectangle A5 = PageSize.A5;

        //System.out.println("A3 \t width: " + A3.getWidth() + "\t height: " + A3.getHeight());
        //System.out.println("A4 \t width: " + A4.getWidth() + "\t height: " + A4.getHeight());
        //System.out.println("A5 \t width: " + A5.getWidth() + "\t height: " + A5.getHeight());


        Rectangle rectangle = reader.getPageSizeWithRotation(1);

//        int rotate = 0;
//        if(rectangle.getHeight() >= rectangle.getWidth()){
//            rotate = PageFormat.PORTRAIT;
//        }else{
//            rotate = PageFormat.LANDSCAPE;
//        }
//
//        System.out.println(rotate);


        Rectangle pdf = reader.getPageSize(1);
        pdf.rotate();
        float height = rectangle.getHeight();
        float width = rectangle.getWidth();

        System.out.println("height:"+height+"\t width:"+width);
        //System.out.println(getPdfPageSizeAlias(width,height));
    }

    public static String getPdfPageSizeAlias(float width, float height) {
        //A3
        if ((width > PageSize.A3.getWidth() - 10 && width < PageSize.A3.getWidth() + 10) && (height > PageSize.A3.getHeight() - 10 && height < PageSize.A3.getHeight() +10)) {
            return "A3";
        }
        //A4
        if ((width > PageSize.A4.getWidth() - 10 && width < PageSize.A4.getWidth() + 10) && (height > PageSize.A4.getHeight() - 10 && height < PageSize.A4.getHeight() +10)) {
            return "A4";
        }
        //A5
        if ((width > PageSize.A5.getWidth() - 10 && width < PageSize.A5.getWidth() + 10) && (height > PageSize.A5.getHeight() - 10 && height < PageSize.A5.getHeight() +10)) {
            return "A5";
        }

        //默认A4
        return "A4";
    }
}
