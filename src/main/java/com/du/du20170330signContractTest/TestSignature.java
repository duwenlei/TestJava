package com.du.du20170330signContractTest;

import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

@SuppressWarnings("all")
public class TestSignature {
	/**
	 * @throws Exception
	 */
	@Test
	public static void verify() throws Exception {
		String certStr = "MIIEdjCCA16gAwIBAgIUJVws2KruCfZeGPlM+t1BeDgPCnUwDQYJKoZIhvcNAQEF\n" +
				"BQAwgaoxCzAJBgNVBAYTAkNOMRIwEAYDVQQIDAnmtZnmsZ/nnIExEjAQBgNVBAcM\n" +
				"CeadreW3nuW4gjE2MDQGA1UECgwt5p2t5bee5biC5YWs5LyX55S15a2Q6K6k6K+B\n" +
				"5bqU55So5pyN5Yqh5Lit5b+DMRgwFgYDVQQLDA93d3cuaHpjYS5vcmcuY24xITAf\n" +
				"BgNVBAMMGOadreW3nuaVsOWtl+iupOivgeS4reW/gzAeFw0xNzAzMzAwNjQ4MjRa\n" +
				"Fw0xODAzMjgxMDMzMzZaMIG4MTAwLgYDVQQKDCfnmbvorrDnrqHnkIbpg6jnvZHk\n" +
				"uIrlip7kuovmtYvor5XljZXkvY0xFzAVBgNVBAsMDuadreW3nkNB5Lit5b+DMRow\n" +
				"GAYDVQQADBFTb2NpYWxTZWN1cml0eU51bTELMAkGA1UEBgwCQ04xEDAOBgNVBAUM\n" +
				"BzAwMTE5NzkxMDAuBgNVBAMMJ+eZu+iusOeuoeeQhumDqOe9keS4iuWKnuS6i+a1\n" +
				"i+ivleWNleS9jTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAI52qeMN\n" +
				"Prq6uvznEOz8hDjWHsi7ONOMLurkj0VQg3RhRwGZ7ctQTxP7ekK8VcTd2MfFAESd\n" +
				"JNtaKPLu+wI0yt0qsceNt6b+mHa0l+HC53Ifn951KW9wsp5Yi+sctm9uJlZOw096\n" +
				"ftX1jXTMJ7EAR9UkLChts6nJCYJAZJdNe2TK9IlDJynSgZz+uLqCW8WwgtInM9/a\n" +
				"YBgrVPOfCnoak8ruzpZPBpXbSPD7OOGM/JG8eQew/Y8xBx8U9J5XBLznLHq7s+zI\n" +
				"dYttSy53E82o16JB1I4Wme1qHsl9B7sV8kvzrOV5o+7M+ZMznGp2WBfPGIXswLuh\n" +
				"xrkr+LTEaw2/DHsCAwEAAaOBgzCBgDAJBgNVHRMEAjAAMAsGA1UdDwQEAwIE8DBm\n" +
				"BgNVHR8EXzBdMFugWaBXhlVodHRwOi8vdG9wY2EuaXRydXMuY29tLmNuL3B1Ymxp\n" +
				"Yy9pdHJ1c2NybD9DQT02OUY4QUYwRTFCRjFFNTA3NzhDNUMyMzIxMkREMDVENThE\n" +
				"RUYxNTlGMA0GCSqGSIb3DQEBBQUAA4IBAQAXi3Luu0yJVXzPs87ITTQ6wxhTUQM0\n" +
				"fba8mDIoAdFKnS1sdZT+SzKY6/eddjwULPEfaM7wyMWCpmcazwumxu13RXHKSNzE\n" +
				"cEPDcDtR6JKd4dGS9xbjQhy3uCY2u9j7WdnF/HjuQfOSE3wm2LmH9FqcN0lwptjj\n" +
				"m2JYTl8bCYSp4IIhXWnBjdeFJnN0oSGA+anwzQi73hEUuWHBAhEw8l/Ung11iqwu\n" +
				"MgfhjlDz7h6lU0kp5zrvVn4uOPLtADCvVM1f12qI55gTwxCldXB1jcooZxPeqopy\n" +
				"rhODw+BifR66cFt7feLGEOX3xYYvcyKZG/UdCuITrOC+rsVaO9yPGSuL";
		String originData = "MV0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMTcwMzMwMDkwNjU4WjAjBgkqhkiG9w0BCQQxFgQUQ3acQfrhFohNIdbifpe8cgT8qio=";
		String signData = "cPIUPn5Bka96jNe+1ydsplq7y9LSL4+3LjaMPaxHq4o+SpBBQPvbWfVqIyn86jA5m2xnlj5QojRCswLc4skXWybzjRHXbRrZ8zL7hkYZMdY5LKDS1XkdX1rklaXd3kqAkMO8mE7wPk9i0UdKtIfkW7DirNAiDm6J6OoRsCfbf/4jM8XBM2Cf/YPq2/+sf1eGHQuSvKFGSo9191hDZla5iXJ8+TtW7vtSZ9fOPGJIIY7/U4osBZ2tM4iogvuq4KfSzIxmFGpPscZuZWQiOFlL99uApF4ti7i9bXwl18bV5gqiXAleWCeA45yxXPhKf7pxVj5Pu9TB4a8EA9Ss32Z49g==";


		CertificateFactory cft = CertificateFactory.getInstance("X.509");
		Certificate cert = cft.generateCertificate(new ByteArrayInputStream(Base64.decode(certStr)));

		Signature sign = Signature.getInstance("SHA1WithRSA");
		sign.initVerify(cert);
		sign.update(originData.getBytes());
		Boolean falg = sign.verify(Base64.decode(signData));
		System.out.println(falg);
	}
	/**
	 * @throws Exception
	 */
	@Test
	public static void verify1() throws Exception {
		String certStr = "MIIEdjCCA16gAwIBAgIUJVws2KruCfZeGPlM+t1BeDgPCnUwDQYJKoZIhvcNAQEF\n" +
				"BQAwgaoxCzAJBgNVBAYTAkNOMRIwEAYDVQQIDAnmtZnmsZ/nnIExEjAQBgNVBAcM\n" +
				"CeadreW3nuW4gjE2MDQGA1UECgwt5p2t5bee5biC5YWs5LyX55S15a2Q6K6k6K+B\n" +
				"5bqU55So5pyN5Yqh5Lit5b+DMRgwFgYDVQQLDA93d3cuaHpjYS5vcmcuY24xITAf\n" +
				"BgNVBAMMGOadreW3nuaVsOWtl+iupOivgeS4reW/gzAeFw0xNzAzMzAwNjQ4MjRa\n" +
				"Fw0xODAzMjgxMDMzMzZaMIG4MTAwLgYDVQQKDCfnmbvorrDnrqHnkIbpg6jnvZHk\n" +
				"uIrlip7kuovmtYvor5XljZXkvY0xFzAVBgNVBAsMDuadreW3nkNB5Lit5b+DMRow\n" +
				"GAYDVQQADBFTb2NpYWxTZWN1cml0eU51bTELMAkGA1UEBgwCQ04xEDAOBgNVBAUM\n" +
				"BzAwMTE5NzkxMDAuBgNVBAMMJ+eZu+iusOeuoeeQhumDqOe9keS4iuWKnuS6i+a1\n" +
				"i+ivleWNleS9jTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAI52qeMN\n" +
				"Prq6uvznEOz8hDjWHsi7ONOMLurkj0VQg3RhRwGZ7ctQTxP7ekK8VcTd2MfFAESd\n" +
				"JNtaKPLu+wI0yt0qsceNt6b+mHa0l+HC53Ifn951KW9wsp5Yi+sctm9uJlZOw096\n" +
				"ftX1jXTMJ7EAR9UkLChts6nJCYJAZJdNe2TK9IlDJynSgZz+uLqCW8WwgtInM9/a\n" +
				"YBgrVPOfCnoak8ruzpZPBpXbSPD7OOGM/JG8eQew/Y8xBx8U9J5XBLznLHq7s+zI\n" +
				"dYttSy53E82o16JB1I4Wme1qHsl9B7sV8kvzrOV5o+7M+ZMznGp2WBfPGIXswLuh\n" +
				"xrkr+LTEaw2/DHsCAwEAAaOBgzCBgDAJBgNVHRMEAjAAMAsGA1UdDwQEAwIE8DBm\n" +
				"BgNVHR8EXzBdMFugWaBXhlVodHRwOi8vdG9wY2EuaXRydXMuY29tLmNuL3B1Ymxp\n" +
				"Yy9pdHJ1c2NybD9DQT02OUY4QUYwRTFCRjFFNTA3NzhDNUMyMzIxMkREMDVENThE\n" +
				"RUYxNTlGMA0GCSqGSIb3DQEBBQUAA4IBAQAXi3Luu0yJVXzPs87ITTQ6wxhTUQM0\n" +
				"fba8mDIoAdFKnS1sdZT+SzKY6/eddjwULPEfaM7wyMWCpmcazwumxu13RXHKSNzE\n" +
				"cEPDcDtR6JKd4dGS9xbjQhy3uCY2u9j7WdnF/HjuQfOSE3wm2LmH9FqcN0lwptjj\n" +
				"m2JYTl8bCYSp4IIhXWnBjdeFJnN0oSGA+anwzQi73hEUuWHBAhEw8l/Ung11iqwu\n" +
				"MgfhjlDz7h6lU0kp5zrvVn4uOPLtADCvVM1f12qI55gTwxCldXB1jcooZxPeqopy\n" +
				"rhODw+BifR66cFt7feLGEOX3xYYvcyKZG/UdCuITrOC+rsVaO9yPGSuL";
		String originData = "1";
		String signData = "QkenIAa+HbxyHOlQdpBIzN/qf1DHjCzGr3kIRO17XKqQkQkiddT+l385j+r3Y/dbTSf2G8OOBvRJG+8j5Y8k1mcxCIeK4c63f759S4NQI6+jUe/POFHWoYVIuj1uMxhd8J2zi3G+aNm8q9SOBml8hIlslbdv0B6/LjMlk2ET07Afrj8II9OCi6Hl3QpfFTwHpoCHxEPKuCDEUDHs9srKb5mt8pRUZovUTiNX3Kme8rip48u5i239nW7yrJQxSsK2DkAPO+YQebCse3oQRdxpbExL/ZuSl6Q4iRVOumwpPpx0nzbI8gvCc9KSjx/PKLdVE9+9cigEFPC3oEtC7aQJTQ==";


		CertificateFactory cft = CertificateFactory.getInstance("X.509");
		Certificate cert = cft.generateCertificate(new ByteArrayInputStream(Base64.decode(certStr)));

		Signature sign = Signature.getInstance("SHA1WithRSA");
		sign.initVerify(cert);
		sign.update(originData.getBytes());
		Boolean falg = sign.verify(Base64.decode(signData));
		System.out.println(falg);
	}

	public static void main(String[] args) throws Exception {
		verify();
	}
}
