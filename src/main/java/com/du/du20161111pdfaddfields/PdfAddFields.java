package com.du.du20161111pdfaddfields;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.BaseField;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.TextField;

public class PdfAddFields {
	private final static String SRC = "D:/签约系统开发者手册v2.pdf";
	private final static String DESC = "D:/签约系统开发者手册v2_desc.pdf";
	private final static String SET = "D:/签约系统开发者手册v2_set.pdf";

	public static void main(String[] args) throws Exception {
		testAddFields(SRC, DESC);
		setField(DESC, SET);
	}

	public static void testAddFields(String src, String desc) throws Exception {
		PdfReader reader = new PdfReader(src);
		PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(desc));

		PdfWriter writer = stamper.getWriter();
		PdfFormField personal = PdfFormField.createEmpty(writer);
		personal.setFieldName("test");
		BaseFont bf = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.EMBEDDED);

		TextField name = new TextField(writer, new Rectangle(0, 0, 200, 30), "name");
		name.setFontSize(30);
		name.setFieldName("name");
		name.setFont(bf);
		name.setTextColor(null);
		name.setText("name");
		name.setVisibility(BaseField.HIDDEN);
		// name.setBox(new Rectangle(0,0,50,30));

		PdfFormField userNameField = name.getTextField();
		personal.addKid(userNameField);

		RadioCheckField radioCheckField = new RadioCheckField(writer,new Rectangle(1,1,1,1),"qq","on");

		// TextField pwd = new TextField(writer, new Rectangle(0, 35, 200, 65),
		// "password");
		// pwd.setFontSize(30);
		// pwd.setDefaultText("pwd");
		// pwd.setFieldName("password");

		// PdfFormField pwdField = pwd.getTextField();
		// personal.addKid(pwdField);

		stamper.addAnnotation(personal, 1);
		stamper.close();
		reader.close();
	}

	public static void setField(String src, String desc) throws Exception {
		OutputStream out = new FileOutputStream(desc);
		PdfReader reader = new PdfReader(src);
		PdfStamper stamper = new PdfStamper(reader, out);

		AcroFields acroFields = stamper.getAcroFields();
		Map fields = acroFields.getFields();

		// UniGB-UCS2-H
		BaseFont bf = BaseFont.createFont("STSong-Light", "UniGB-UTF8-V", BaseFont.EMBEDDED);
		Iterator keySet = fields.keySet().iterator();
		while (keySet.hasNext()) {
			String key = keySet.next().toString();
			// System.out.println(stamper.getAcroFields().setFieldProperty(key,
			// "textfont", bf, null));
			System.out.println(stamper.getAcroFields().setField(key, "都闻雷ssssssssssssssssssssssssssssssssssss"));
		}
		System.out.println("acro fields :" + acroFields.getFields().toString());

		stamper.setFormFlattening(true);
		stamper.setFreeTextFlattening(true);
		stamper.close();
		reader.close();
	}

	public void itextDemo() throws Exception {
		String src = DESC;
		String dest = "D:/desc.pdf";
		PdfReader reader = new PdfReader(src);
		PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
		AcroFields form = stamper.getAcroFields();
		form.setField("name", "Bruno Lowagie");
		// form.setFieldProperty("text_2", "fflags", 0, null);
		// form.setFieldProperty("text_2", "bordercolor", BaseColor.RED, null);
		form.setField("password", "bruno");
		// form.setFieldProperty("text_3", "clrfflags", TextField.PASSWORD,
		// null);
		// form.setFieldProperty("text_3", "setflags",
		// PdfAnnotation.FLAGS_PRINT, null);
		// form.setField("text_3", "12345678", "xxxxxxxx");
		// form.setFieldProperty("text_4", "textsize", new Float(12), null);
		// form.regenerateField("text_4");
		stamper.close();
		reader.close();
	}

	public void test() throws Exception {
		List<FieldInfo> fs = new ArrayList<>();

		FieldInfo f1 = new FieldInfo();
		f1.setType("TextField");
		f1.setName("test");
		f1.setTextColor("sssss");
		f1.setLlx(0);
		f1.setLly(0);
		f1.setUrx(20);
		f1.setUry(20);
		f1.setPage(1);
		fs.add(f1);

		PdfReader reader = new PdfReader(SRC);
		PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(DESC));

		createPdfFields(stamper, fs);
		stamper.close();
		reader.close();

	}

	public static void createPdfFields(PdfStamper stamper, List<FieldInfo> fields) throws Exception {
		PdfWriter writer = stamper.getWriter();
		for (FieldInfo info : fields) {
			PdfFormField form = PdfFormField.createEmpty(writer);
			form.setFieldName("test");
			BaseField field = getFileType(writer, info);

			field.setText(info.getText());
			if (info.getTextColor() != null) {
				field.setTextColor(getTextColor(info.getTextColor()));
			}
			if (info.getFontName() != null && info.getEncoding() != null) {
				field.setFont(getFont(info.getFontName(), info.getEncoding()));
			}
			if (info.getFontSize() > 0) {
				field.setFontSize(info.getFontSize());
			}
			if (info.getVisibility() > 0) {
				field.setVisibility(info.getVisibility());
			}

			switch (info.getType()) {
			case "TextField":
				form.addKid(((TextField) field).getTextField());
				break;
			case "RadioCheckField":
				form.addKid(((RadioCheckField) field).getCheckField());
				break;
			default:
				throw new RuntimeException("Field type conversion failed 'BaseField to " + info.getType() + "'");
			}
			stamper.addAnnotation(form, info.getPage());
		}
	}

	private static BaseFont getFont(final String fontName, final String encoding) throws Exception {
		BaseFont font = null;
		switch (fontName) {
		case "STSong-Light":
			font = BaseFont.createFont(fontName, encoding, BaseFont.EMBEDDED);
			break;
		}
		return font;
	}

	private static BaseColor getTextColor(String colorStr) {
		BaseColor color = null;
		switch (colorStr) {
		case "WHITE":
			color = BaseColor.WHITE;
			break;
		case "LIGHT_GRAY":
			color = BaseColor.LIGHT_GRAY;
			break;
		case "DARK_GRAY":
			color = BaseColor.DARK_GRAY;
			break;
		case "BLACK":
			color = BaseColor.BLACK;
			break;
		case "RED":
			color = BaseColor.RED;
			break;
		case "PINK":
			color = BaseColor.PINK;
			break;
		case "ORANGE":
			color = BaseColor.ORANGE;
			break;
		case "YELLOW":
			color = BaseColor.YELLOW;
			break;
		case "GREEN":
			color = BaseColor.GREEN;
			break;
		case "MAGENTA":
			color = BaseColor.MAGENTA;
			break;
		case "CYAN":
			color = BaseColor.CYAN;
			break;
		case "BLUE":
			color = BaseColor.BLUE;
			break;
		default:
			throw new RuntimeException("failed to create text color  '" + colorStr + "'");

		}
		return color;
	}

	private static BaseField getFileType(PdfWriter writer, FieldInfo info) {
		BaseField field = null;
		String type = info.getType();
		Rectangle box = new Rectangle(info.getLlx(), info.getLly(), info.getUrx(), info.getUry());
		switch (type) {
		case "TextField":
			field = new TextField(writer, box, info.getName());
			break;
		case "RadioCheckField":
			field = new RadioCheckField(writer, box, info.getName(), "false");
			break;
		default:
			throw new RuntimeException("failed to create field  '" + type + "'");
		}
		return field;
	}

}

class FieldInfo {
	static final String[] types = { "TextField", "RadioCheckField" };
	static final String[] colors = { "WHITE", "LIGHT_GRAY", "GRAY,DARK_GRAY", "BLACK", "RED", "PINK", "ORANGE",
			"YELLOW", "GREEN", "MAGENTA", "CYAN", "BLUE", "FACTOR" };
	static final String[] visibilitys = { "VISIBLE", "HIDDEN", "VISIBLE_BUT_DOES_NOT_PRINT", "HIDDEN_BUT_PRINTABLE" };
	static final String[] fonts = { "STSong-Light" };
	static final String[] encodings = { "UniGB-UCS2-H", "UniGB-UTF8-V" };

	private String type;
	private String name;
	private String text; // 只有设置了Form表单名称才会显示text的内容
	private String textColor; // [WHITE,LIGHT_GRAY,GRAY,DARK_GRAY,BLACK,RED,PINK,ORANGE,YELLOW,GREEN,MAGENTA,CYAN,BLUE,FACTOR]
	private String fontName;
	private String encoding;
	private String formName;

	private int llx;
	private int lly;
	private int urx;
	private int ury;

	private float fontSize;
	private int visibility;
	private int page;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTextColor() {
		return textColor;
	}

	public void setTextColor(String textColor) {
		this.textColor = textColor;
	}

	public String getFontName() {
		return fontName;
	}

	public void setFontName(String fontName) {
		this.fontName = fontName;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public int getLlx() {
		return llx;
	}

	public void setLlx(int llx) {
		this.llx = llx;
	}

	public int getLly() {
		return lly;
	}

	public void setLly(int lly) {
		this.lly = lly;
	}

	public int getUrx() {
		return urx;
	}

	public void setUrx(int urx) {
		this.urx = urx;
	}

	public int getUry() {
		return ury;
	}

	public void setUry(int ury) {
		this.ury = ury;
	}

	public float getFontSize() {
		return fontSize;
	}

	public void setFontSize(float fontSize) {
		this.fontSize = fontSize;
	}

	public int getVisibility() {
		return visibility;
	}

	public void setVisibility(int visibility) {
		this.visibility = visibility;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	@Test
	public void test(){
		FieldInfo f1 = new FieldInfo();
		f1.setType("TextField");
		f1.setName("test");
		f1.setTextColor("sssss");
		f1.setLlx(0);
		f1.setLly(0);
		f1.setUrx(20);
		f1.setUry(20);
		System.out.println(f1.getFontSize());
	}
}