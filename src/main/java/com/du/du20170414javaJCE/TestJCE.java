package com.du.du20170414javaJCE;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.*;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.cert.*;
/**
 * Created by duwenlei on 17-4-14.
 */
public class TestJCE {
    public static PrivateKey priKey = null;
    public static PublicKey pubKey = null;
    public static SecretKey secretKey = null;

    public static String signMessage(String plainText) throws NoSuchAlgorithmException, UnsupportedEncodingException, SignatureException, InvalidKeyException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        priKey = keyPair.getPrivate();
        pubKey = keyPair.getPublic();
        Signature signature = Signature.getInstance("SHA256WithRSA");
        signature.initSign(priKey);
        signature.update(plainText.getBytes("utf-8"));
        byte[] signData = signature.sign();
        return Base64.encodeBase64String(signData);
    }

    public static boolean verifyMessage(String plainText, String signB64Data) throws NoSuchAlgorithmException, UnsupportedEncodingException, SignatureException, InvalidKeyException {
        Signature signature = Signature.getInstance("SHA256WithRSA");
        signature.initVerify(pubKey);
        signature.update(plainText.getBytes("utf-8"));
        return signature.verify(Base64.decodeBase64(signB64Data));
    }

    public static String encodeMessage(String plainText) throws NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(new SecureRandom());
        secretKey = keyGenerator.generateKey();
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        cipher.update(plainText.getBytes("utf-8"));
        return Base64.encodeBase64String(cipher.doFinal());
    }

    public static String decodeMessage(String encB64Data) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, UnsupportedEncodingException {
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        cipher.update(Base64.decodeBase64(encB64Data));
        return new String(cipher.doFinal(), "utf-8");
    }

    public static void initCert(String certB64) throws CertificateException {
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        X509Certificate cert = (X509Certificate) certificateFactory.generateCertificate(new ByteArrayInputStream(Base64.decodeBase64(certB64)));

        System.out.println("证书序列号：" + cert.getSerialNumber());
        System.out.println("证书类型：" + cert.getType());
        System.out.println("证书主题项：" + cert.getSubjectDN());
        System.out.println("证书颁发者：" + cert.getIssuerDN());
        System.out.println("证书签名算法：" + cert.getSigAlgName());
        System.out.println("证书公钥："+ Base64.encodeBase64String(cert.getPublicKey().getEncoded()));
    }


    public static void main(String[] args) {
        try {
            Sakyamuni.outSakyamuni();
            System.out.println("\t1、签名验签");
            System.out.println("开始签名....");
            String plainText = "杜文磊";
            String signB64Data = signMessage(plainText);
            System.out.println("签名结果：" + signB64Data);
            boolean flag = verifyMessage(plainText, signB64Data);
            System.out.println("开始验签....");
            System.out.println("验证是否通过：" + flag);
            System.out.println("签名验签结束\t end");

            System.out.println();
            System.out.println();


            System.out.println("\t2、加密解密");
            String encData = encodeMessage(plainText);
            System.out.println("开始加密....");
            System.out.println("加密后数据：['" + plainText + "']" + encData);
            System.out.println("开始解密....");
            String decData = decodeMessage(encData);
            System.out.println("解密后数据：" + decData);


            System.out.println();
            System.out.println();


            String certB64 = "" +
                    "MIIDAjCCAmsCEH3Z/gfPqB63EHln+6eJNMYwDQYJKoZIhvcNAQEFBQAwgcExCzAJ\n" +
                    "BgNVBAYTAlVTMRcwFQYDVQQKEw5WZXJpU2lnbiwgSW5jLjE8MDoGA1UECxMzQ2xh\n" +
                    "c3MgMyBQdWJsaWMgUHJpbWFyeSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eSAtIEcy\n" +
                    "MTowOAYDVQQLEzEoYykgMTk5OCBWZXJpU2lnbiwgSW5jLiAtIEZvciBhdXRob3Jp\n" +
                    "emVkIHVzZSBvbmx5MR8wHQYDVQQLExZWZXJpU2lnbiBUcnVzdCBOZXR3b3JrMB4X\n" +
                    "DTk4MDUxODAwMDAwMFoXDTI4MDgwMTIzNTk1OVowgcExCzAJBgNVBAYTAlVTMRcw\n" +
                    "FQYDVQQKEw5WZXJpU2lnbiwgSW5jLjE8MDoGA1UECxMzQ2xhc3MgMyBQdWJsaWMg\n" +
                    "UHJpbWFyeSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eSAtIEcyMTowOAYDVQQLEzEo\n" +
                    "YykgMTk5OCBWZXJpU2lnbiwgSW5jLiAtIEZvciBhdXRob3JpemVkIHVzZSBvbmx5\n" +
                    "MR8wHQYDVQQLExZWZXJpU2lnbiBUcnVzdCBOZXR3b3JrMIGfMA0GCSqGSIb3DQEB\n" +
                    "AQUAA4GNADCBiQKBgQDMXtERXVxp0KvTuWpMmR9ZmDCOFoUgRm1HP9SFIIThbbP4\n" +
                    "pO0M8RcPO/mn+SXXwc+EY/J8Y8+iR/LGWzOOZEAEaMGAuWQcRXfH2G71lSk8UOg0\n" +
                    "13gfqLptQ5GVj0VXXn7F+8qkBOvqlzdUMG+7AUcyM83cV5tkaWH4mx0ciU9cZwID\n" +
                    "AQABMA0GCSqGSIb3DQEBBQUAA4GBAFFNzb5cy5gZnBWyATl4Lk0PZ3BwmcYQWpSk\n" +
                    "U01UbSuvDV1Ai2TT1+7eVmGSX6bEHRBhNtMsJzzoKQm5EWR0zLVznxxIqbxhAe7i\n" +
                    "F6YM40AIOw7n60RzKprxaZLvcRTDOaxxp5EJb+RxBrO6WVcmeQD2+A2iMzAo1KpY\n" +
                    "oJ2daZH9\n" +
                    "";
            System.out.println("\t3、读取证书信息");
            initCert(certB64);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        }
    }
}
