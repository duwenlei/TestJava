package com.du.du20170717TPS;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by duwenlei on 2017-7-17.
 */
public class OkHttpUtil {
    private static OkHttpClient okHttpclient = null;

    static {
        okHttpclient = new OkHttpClient.Builder()
                .connectTimeout(3, TimeUnit.SECONDS)
                .build();
    }

    public static String run(String url) throws Exception {
        Request request = new Request.Builder().url(url).build();
        Response response = okHttpclient.newCall(request).execute();
        if (!response.isSuccessful()) {
            throw new IOException("Unexpected code " + response);
        }
        return response.body().string();

    }
}
