package com.du.du20161107keystore;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.sun.org.apache.xml.internal.security.utils.Base64;

public class TestKeyStore {
	
	public static void main(String[] args) throws Exception {
		Security.addProvider(new BouncyCastleProvider());

		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
		keyPairGenerator.initialize(1024);
		KeyPair keyPair = keyPairGenerator.generateKeyPair();
		PrivateKey priKey = keyPair.getPrivate();
		PublicKey pubKey = keyPair.getPublic();
		
		String certStr = "MIIDbTCCAlWgAwIBAgIUevF4VCrBJuMGnnV3Tk49KDKmbFMwDQYJKoZIhvcNAQEFBQAwgYkxMzAxBgNVBAMMKuWkqeWogeivmuS/oeazqOWGjOeUqOaIt+ivgeS5pu+8iOa1i+ivle+8iTEhMB8GA1UECwwY5aSp5aiB6K+a5L+h77yI5rWL6K+V77yJMSIwIAYDVQQKDBnov5Dnu7Tpg6h1c2Vy77yI5rWL6K+V77yJMQswCQYDVQQGEwJDTjAeFw0xNjEwMzExMTQ2NTJaFw0xNzEwMzExMTQ2NTJaMFAxEzARBgNVBAAMClJlc2lkZW50SUQxCzAJBgNVBAYMAkNOMRswGQYDVQQFDBI1MzEyMDgxOTkxMTAxNzgyMngxDzANBgNVBAMMBueLkOetljCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAx0gbpbWcJWq79REnIeXE9GdjrieN8y7e2gSOaVNgZ3mrzCfbyL7smmjX5We+yiP9BdB37wIpkE1TzzzYZdCzX2YNC2i6kvPTQFpvfaCyIVLNToWLAa6T178NP6XH66cGMBN5/qz58hbFIiUmyalEsKo9uHeA/lr+lI0kBZnhWnECAwEAAaOBiDCBhTAJBgNVHRMEAjAAMAsGA1UdDwQEAwIE8DBrBgNVHR8EZDBiMGCgXqBchlpodHRwOi8vdG9wY2EtdGVzdC5pdHJ1cy5jb20uY24vcHVibGljL2l0cnVzY3JsP0NBPTRFQkE1MEZCN0NERUVEMDU1MzRBOEQzMkM5NENDREQyNDYwMUUyMDgwDQYJKoZIhvcNAQEFBQADggEBAGkmmaFQcAA79WHEsMbMG1S4XHU0ozGeIJ3fLGhbcFg4emjhJdGvq+OZIvNyh9qnm3e38Nx/8C0yRcSIzsUew4baoFyenUH39fBUERkm1Kvadi1ZcSBCjkmgrljhMIquJyeRBHsg0OF8WrhxLccVPxBTohQEv3FS0vwqgHeRFclfuPMFsC32/dpG8rnBjMoQOVPYJkw8JwhdAIPePVzyDER4BYfNdTWU8oRz72k57Z84rEU4YgZofzlNyWVfvremehprx5aGU2wbWfZJi18nMZXYstAURN0B6krUiOX0gKyaR/Ki0lgdpwArlu2Ct53Aytn+DEqGljmLcD+Kqc6lOUs=";
		CertificateFactory cft = CertificateFactory.getInstance("X.509");
		Certificate cert = cft.generateCertificate(new ByteArrayInputStream(Base64.decode(certStr)));
		
		KeyStore keyStore = KeyStore.getInstance("JKS");
		keyStore.load(null, "password".toCharArray());
		keyStore.setKeyEntry("1", priKey, "password".toCharArray(), new Certificate[]{cert});
		keyStore.setCertificateEntry("2", cert);
		keyStore.store(new FileOutputStream(new File("D:/key.jks")), "password".toCharArray());
		
	}
}
