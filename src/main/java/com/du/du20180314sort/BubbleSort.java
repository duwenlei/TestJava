package com.du.du20180314sort;

/**
 * 冒泡排序
 * @author duwenlei
 */
public class BubbleSort {
    public static void bubbleSort(int[] array) {

        for (int a : array) {
            System.out.print(a + "\t");
        }
        System.out.println();

        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] > array[j]) {
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
            for (int a : array) {
                System.out.print(a + "\t");
            }
            System.out.println();
        }

        System.out.println("-----------------------");
    }

    public static void main(String[] args) {
        int[] array = {89, 34, 12, 45, 67, 1, 95, 76, 78, 23, 5, 1};
        bubbleSorts(array);
        bubbleSort(array);
        for (int a : array) {
            System.out.print(a + "\t");
        }
    }

    public static void bubbleSorts(int[] array) {
        int size = array.length;
        int count = 0;
        for (int i = 0; i < size - 1; i++) {
            for (int j = 0; j < size - 1 - i; j++){
                if (array[j] > array[j+1]){
                    int temp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = temp;
                }
            }
        }
    }
}
