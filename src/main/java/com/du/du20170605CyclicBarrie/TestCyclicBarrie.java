package com.du.du20170605CyclicBarrie;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.*;

/**
 * Created by duwenlei on 17-6-5.
 */
public class TestCyclicBarrie {
    public static void main(String[] args) {
//        ExecutorService newCachedThreadPool = Executors.newCachedThreadPool();
//        CyclicBarrier cyclicBarrier = new CyclicBarrier(3);
//        for (int i = 0; i < 3; i++) {
//            Runnable runnable = new Runnable() {
//                @Override
//                public void run() {
//                    try {
//                        //Thread.sleep(new Random().nextInt(1000));
//                        System.out.println("1---线程" + Thread.currentThread().getName() + "即将到达1,当前有" + (cyclicBarrier.getNumberWaiting() + 1) + "" + (cyclicBarrier.getNumberWaiting() == 2 ? ",都到齐了，继续走" : ",正在等待"));
//                        cyclicBarrier.await();
//
//                        //Thread.sleep(new Random().nextInt(1000));
//                        System.out.println("2---线程" + Thread.currentThread().getName() + "即将到达2,当前有" + (cyclicBarrier.getNumberWaiting() + 1) + "" + (cyclicBarrier.getNumberWaiting() == 2 ? ",都到齐了，继续走" : ",正在等待"));
//                        cyclicBarrier.await();
//
//                        //Thread.sleep(new Random().nextInt(1000));
//                        System.out.println("3---线程" + Thread.currentThread().getName() + "即将到达3,当前有" + (cyclicBarrier.getNumberWaiting() + 1) + "" + (cyclicBarrier.getNumberWaiting() == 2 ? ",都到齐了，继续走" : ",正在等待"));
//                        cyclicBarrier.await();
//
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    } catch (BrokenBarrierException e) {
//                        e.printStackTrace();
//                    }
//                }
//            };
//            newCachedThreadPool.execute(runnable);
//        }
//        newCachedThreadPool.shutdown();

        Builder builder = new Builder();
        builder.test1("12").test2("34");
        System.out.println(builder.sb);

        Builder builder1 = new Builder();
        builder1.test1("34").test2("12");
        System.out.println(builder1.sb);

        Date startDay = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDay);
        calendar.add(Calendar.DAY_OF_MONTH,3);
        calendar.add(Calendar.DAY_OF_MONTH,3);
        System.out.println(startDay.toLocaleString());
        System.out.println(calendar.getTime().toLocaleString());
    }
}

class Builder{

    public String sb = "";
    public Builder test1(String str){
        sb+=str;
        return this;
    }

    public Builder test2(String str){
        sb+=str;
        return this;
    }

    public Builder test3(String str){
        sb+=str;
        return this;
    }

}
