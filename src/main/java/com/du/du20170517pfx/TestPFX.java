//package com.du.du20170517pfx;
//
//import cn.tca.TopBasicCrypto.asn1.pkcs.Pfx;
//import cn.topca.security.sm.SMKeyStore;
//import cn.topca.security.sm.TopSMProvider;
//import cn.topca.security.util.KeyStoreUtil;
//import org.bouncycastle.jce.provider.BouncyCastleProvider;
//
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.security.KeyStore;
//import java.security.KeyStoreException;
//import java.security.NoSuchAlgorithmException;
//import java.security.Security;
//import java.security.cert.CertificateException;
//
///**
// * Created by duwenlei on 17-5-17.
// */
//public class TestPFX {
//    public static void main(String[] args) {
//        //Security.addProvider(new TopSMProvider());
//        Security.addProvider(new BouncyCastleProvider());
//        try {
//            InputStream in = new FileInputStream("/home/duwenlei/Downloads/server_sm2.p12");
//            char[] password =  "11111111".toCharArray();
//            KeyStore ks = KeyStore.getInstance("Pkcs12",new TopSMProvider());
//            ks.load(in,password);
//        } catch (KeyStoreException e) {
//            e.printStackTrace();
//        } catch (CertificateException e) {
//            e.printStackTrace();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//}
