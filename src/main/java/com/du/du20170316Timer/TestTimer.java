package com.du.du20170316Timer;


import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
/**
 * Created by duwenlei on 17-3-16.
 */
public class TestTimer {
    public static void main(String[] args) {
//        Calendar calendar = Calendar.getInstance();
//        calendar.set(Calendar.HOUR_OF_DAY,24);
//        Timer timer = new Timer("cleanContractCache");
//        timer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//
//            }
//        },calendar.getTime(),1000*60*60*24);

        try {
            String contractPath = "/home/duwenlei/.esp/contracts";
            Path path = Paths.get(contractPath);
            File tf = path.toFile();
            File[] files = tf.listFiles();
            for (File file : files){
                deleteDir(file);
            }
        } catch (Exception e) {
            System.err.println("删除合同模板缓存文件失败：" + e.getMessage());
        }
    }
    static void deleteDir(File file){
        if (file.isDirectory()){
            File[] files = file.listFiles();
            for (File f : files){
                deleteDir(f);
            }
        }
        file.delete();
    }
}
