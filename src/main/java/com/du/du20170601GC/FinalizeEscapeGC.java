package com.du.du20170601GC;

/**
 * 此代码只演示了两点
 * 1、对象可以在被GC时自我拯救。
 * 2、这种自救方式只有一次，因为一个对象的finalize()最多被执行一次。
 * Created by duwenlei on 2017-6-2.
 */
public class FinalizeEscapeGC {
    public static FinalizeEscapeGC SAVE_HOOK = null;

    public void isAlive(){
        System.out.println("yes, I am still alive");
    }

    public static void main(String[] args) throws InterruptedException {
        SAVE_HOOK = new FinalizeEscapeGC();

        //对象第一次拯救自己
        SAVE_HOOK = null;
        System.gc();
        Thread.sleep(500);//因为finalize方法优先级很低，所以先暂停500毫秒
        if (SAVE_HOOK != null){
            SAVE_HOOK.isAlive();
        }else{
            System.out.println("no i am dead");
        }

        //第二次没能拯救
        SAVE_HOOK = null;
        System.gc();
        Thread.sleep(500);
        if (SAVE_HOOK != null){
            SAVE_HOOK.isAlive();
        }else{
            System.out.println("no i am dead");
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("finalize is executed");
        SAVE_HOOK = this;
    }
}
