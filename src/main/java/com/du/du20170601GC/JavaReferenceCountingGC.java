package com.du.du20170601GC;

/**
 * -XX:+PrintGCDetails
 * -XX:+PrintGCDateStamps
 * -XX:+PrintGCTimeStamps
 * Created by duwenlei on 2017-6-1.
 */
public class JavaReferenceCountingGC {
    public Object instance = null;
    private static final int _1MB = 1024 * 1024;

    private byte[] bigSize = new byte[2* _1MB];

    public static void main(String[] args) {
        JavaReferenceCountingGC rA = new JavaReferenceCountingGC();
        JavaReferenceCountingGC rB = new JavaReferenceCountingGC();
        rA.instance = rB;
        rB.instance = rA;

        rA = null;
        rB = null;

        System.gc();
    }
}
