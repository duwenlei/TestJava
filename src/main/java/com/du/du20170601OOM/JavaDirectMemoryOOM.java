package com.du.du20170601OOM;

import sun.misc.Unsafe;

import java.lang.reflect.Field;

/**
 * 本机内存溢出
 * -Xmx20M -XX:MaxDirectMemorySize=10M
 * Created by duwenlei on 2017-5-31.
 */
public class JavaDirectMemoryOOM {
    private static final int _1MB = 1024 * 1024;

    public static void main(String[] args) throws Exception {
        Field unsafeField = Unsafe.class.getDeclaredFields()[0];
        unsafeField.setAccessible(true);

        Unsafe unsafe = (Unsafe) unsafeField.get(null);
        while (true){
            System.out.println(1);
            unsafe.allocateMemory(_1MB);
        }
    }
}
