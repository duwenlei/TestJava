package com.du.du20170601OOM;

/**
 * -Xss128k
 * Created by duwenlei on 2017-5-31.
 */
public class JavaVMStackSOF {
    private int stackLength = 1;

    public void stackLeak() {
        stackLength++;
        stackLeak();
    }

    public static void main(String[] args) throws Throwable{
        JavaVMStackSOF j = new JavaVMStackSOF();
        try {
            j.stackLeak();
        } catch (Throwable e) {
            System.out.println("stack Length: " + j.stackLength);
            throw e;
        }


    }
}
