package com.du.du20170601OOM;

/**
 * 虚拟机栈和本地栈内存溢出
 * -Xss2m
 * Created by duwenlei on 2017-5-31.
 */
public class JavaVMStackOOMForThread {
    public void dontStop(){
        while (true){

        }
    }
    public void statrThread(){
        while (true){
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    dontStop();
                }
            });
            t.start();
        }
    }

    public static void main(String[] args) {
        JavaVMStackOOMForThread j = new JavaVMStackOOMForThread();
        j.statrThread();
    }
}
