package com.du.du20170601OOM;

import javafx.beans.binding.StringBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * JAVA方法区和常量池溢出
 * -XX:PermSize=10M -XX:MaxPermSize=10M
 * Created by duwenlei on 2017-5-31.
 */
public class JavaRuntimeConstanPoolOOM {

    /**
     * Java 1.7 不会得到结果，1.6会得到一个 OutOfMemoryError : Perm space
     * */
    public static void test() {
        List list = new ArrayList();
        int i = 0;
        while (true){
            list.add(String.valueOf(i++).intern());
        }
    }

    public static void main(String[] args) {
        String str1 = new StringBuilder("计算机").append("软件").toString();
        System.out.println(str1.intern() == str1);

        String str2 = new StringBuilder("ja").append("va").toString();
        System.out.println(str2.intern() == str2);
    }
}
