package com.du.du20170823luoluo;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by duwenlei on 17-8-24.
 */
public class Matching {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int count = Integer.valueOf(sc.next());
        if (count < 0 || count > 100) {
            return;
        }
        List<String> result = new ArrayList<String>();
        boolean flag = true;
        for (int i = 1; i <= count; i++) {
            String str = sc.next();
            if (str != null && str.length() < 10000) {
                try {
                    result.add(match(str));
                } catch (IllegalArgumentException e) {
                    flag = false;
                    break;
                }
            } else {
                return;
            }
        }
        sc.close();
        if (flag) {
            for (String str : result) {
                System.out.println(str);
            }
        }
    }

    public static String match(String data) {
        int leftSquareBracket = 0;
        int rightSquareBracket = 0;

        int leftBracket = 0;
        int rightBracket = 0;

        char[] chars = data.toCharArray();
        for (char c : chars) {
            switch (c) {
                case '[':
                    leftSquareBracket++;
                    break;
                case ']':
                    rightSquareBracket++;
                    break;
                case '(':
                    leftBracket++;
                    break;
                case ')':
                    rightBracket++;
                    break;
                default:
                    throw new IllegalArgumentException("错误的符号");
            }
        }
        String flag = "No";
        if ((leftBracket == rightBracket) && (leftSquareBracket == rightSquareBracket)) {
            flag = "Yes";
        }
        return flag;
    }
}
