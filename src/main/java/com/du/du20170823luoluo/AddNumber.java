package com.du.du20170823luoluo;

import java.util.Scanner;

/**
 * Created by duwenlei on 17-8-23.
 */
public class AddNumber {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int a = Integer.valueOf(sc.next());
        int b = Integer.valueOf(sc.next());

        System.out.println(a + b);
    }
}