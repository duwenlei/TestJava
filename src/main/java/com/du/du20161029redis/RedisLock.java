package com.du.du20161029redis;

import redis.clients.jedis.Jedis;

import java.util.Collections;

public class RedisLock {
    private static final String SCRIPT = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";

    private static final String LOCK_NAME = "LOCK";

    private static final String LOCK_SUCCESS = "OK";
    private static final String SET_IF_NOT_EXIST = "NX";
    private static final String SET_WITH_EXPIRE_TIME = "PX";

    /**
     * 加锁
     *
     * @return
     */
    public static boolean lock(Jedis jedis, String value, int expireTime) {
        String result = jedis.set(LOCK_NAME, value, SET_IF_NOT_EXIST, SET_WITH_EXPIRE_TIME, expireTime);
        if (LOCK_SUCCESS.equals(result)) {
            return true;
        }
        return false;
    }

    /**
     * 解锁
     */
    public static boolean releaseLock(Jedis jedis, String value) {
        Object result = jedis.eval(SCRIPT, Collections.singletonList(LOCK_NAME), Collections.singletonList(value));
        if (LOCK_SUCCESS.equals(result)) {
            return true;
        }
        return false;
    }
}
