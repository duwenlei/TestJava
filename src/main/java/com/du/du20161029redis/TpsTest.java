package com.du.du20161029redis;



import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TpsTest {
    public static long t1 = Long.parseLong("0");

    public static void tps(int tpools, int tasks) throws InterruptedException {
        CountDownLatch latchStart = new CountDownLatch(tpools);
        CountDownLatch latchEnd = new CountDownLatch(tpools);
        ExecutorService executor = Executors.newFixedThreadPool(tpools);
        for (int i = 0; i < tpools; i++) {
            Runnable task = new Runnable() {
                @Override
                public void run() {
                    latchStart.countDown();
                    for (int j = 0; j < tasks; j++) {
                        try {
                            Service.get();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    latchEnd.countDown();
                }
            };
            executor.execute(task);
        }
        executor.shutdown();
        latchStart.await();
        long t1 = System.currentTimeMillis();
        latchEnd.await();
        long t2 = System.currentTimeMillis();

        double taskCount = tpools * tasks;
        double consuming = t2 - t1;
        System.out.println("开始时间        : " + t1);
        System.out.println("结束时间        : " + t2);
        System.out.println("总任务数        : " + taskCount);
        System.out.println("总耗时          : " + consuming + "ms");
        System.out.println("TPS            : " + (taskCount / (consuming / 1000.0)) + "次/ms");
    }

    public static void main(String[] args) throws InterruptedException {
        tps(100, 100);
    }
}
