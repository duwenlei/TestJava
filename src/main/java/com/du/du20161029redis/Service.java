package com.du.du20161029redis;

import redis.clients.jedis.Jedis;

import java.util.UUID;

public class Service {
    private static int INDEX = 0;

    public static void get() {
        Jedis jedis = RedisJava.getInstance();
        String value = UUID.randomUUID().toString();
        if (RedisLock.lock(jedis, value, 1000)) {
            System.out.println(INDEX);
            INDEX = INDEX + 1;
            RedisLock.releaseLock(jedis, value);
        }
        jedis.close();
    }
}
