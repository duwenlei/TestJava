package com.du.du20161029redis;

import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("resource")
public class RedisJava {
    private static final String HOST = "192.168.1.150";
    private static final int PORT = 6379;


    @Test
    public void testRedisServiceIsRunning() {
        Jedis jedis = new Jedis(HOST, PORT);

        //DENIED Redis is running in protected mode because protected mode is enabled,
        //no bind address was specified, no authentication password is requested to clients.
        //遇到上面的错误提示
        //执行 CONFIG SET protected-mode no
        //
        System.out.println("redis is running " + jedis.ping());
    }

    private static JedisPool jedisPool = null;

    public static Jedis getInstance() {
        Jedis jedis = null;
        if (jedisPool == null) {
            jedisPool = new JedisPool(HOST, PORT);
        }
        return jedisPool.getResource();
    }

    @Test
    public void testRedisGetKey() {
        JedisPool jedisPool = new JedisPool(HOST, PORT);
        Jedis jedis = jedisPool.getResource();
        jedis.set("A", "Redis tutorial");

        System.out.println("redis get key A : " + jedis.get("A"));
    }


    public static void main(String[] args) {
        System.out.println("三生三世");
        Map<String, String> map = new HashMap<>();
        map.put("a", "checkSignatureResult");
        map.put("b", "println");
        map.put("c", "void");
        map.put("d", "System");
        map.put("e", "testRedisServiceIsRunning");
        map.put("f", "Jedis");
        map.put("g", "System.out.println(redis get key A :  + jedis.get());");

        long startTime = System.currentTimeMillis();
        System.out.println(map.toString());

        while (map.toString().length() > 100) {
            Set<String> keySet = map.keySet();
            String key = keySet.iterator().next();
            map.remove(key);
        }

        System.out.println(map.toString());
        System.out.println(System.currentTimeMillis() - startTime + "ms");
    }
}
