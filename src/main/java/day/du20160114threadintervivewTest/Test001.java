package day.du20160114threadintervivewTest;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Test001 {

	public static void main(String[] args) {
		ExecutorService service = Executors.newFixedThreadPool(4);
		for (int i = 0; i < 3; i++) {
			Runnable command = new Runnable() {
				public void run() {
					int j = 0;
					System.out.println("begin"+System.currentTimeMillis()/1000);
					for (int i = 0; i < 16; i++) {
						if(j==4){
							break;
						}
						final String log =""+ (i+1);
						printLog(log);
						++j;
					}
				}
			};
			service.execute(command);
		}
		service.shutdown();
		int j = 0;
		System.out.println("begin"+System.currentTimeMillis()/1000);
		for (int i = 0; i < 16; i++) {
			if(j==4){
				break;
			}
			final String log =""+ (i+1);
			printLog(log);
			++j;
		}
	}
	public static void printLog(String log){
		System.out.println(log+" "+System.currentTimeMillis()/1000);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
