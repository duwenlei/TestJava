package day.du20160114threadintervivewTest;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Test001_1 {

	public static void main(String[] args) {
		final BlockingQueue<String> queue = new ArrayBlockingQueue<String>(16);
		for (int i = 0; i < 4; i++) {
			new Thread(new Runnable() {
				public void run() {
					int j = 0;
					while (j!=4) {
						try {
							Test001_1.printLog(queue.take());
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						j++;
					}
				}
			}).start();
		}

		System.out.println("begin" + System.currentTimeMillis() / 1000);
		for (int i = 0; i < 16; i++) {
			final String log = "" + (i + 1);
			try {
				queue.put(log);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public static void printLog(String log) {
		System.out.println(log + " " + System.currentTimeMillis() / 1000);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
