package day.du20170928webServer;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by duwenlei on 2017-9-28.
 */
public class WebServer {

    public static void main(String[] args) throws Exception {

        ServerSocket server = new ServerSocket(9999);
        Socket sock = server.accept();

        System.out.println("wait");
        InputStream inputStream = sock.getInputStream();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        int len1 ;
        byte[] buffer1 = new byte[1024];
        while((len1 = inputStream.read(buffer1)) > 0){

            outputStream.write(buffer1,0,len1);
            outputStream.flush();

        }

        byte[] inDataByte = outputStream.toByteArray();
        String inData =  new String(inDataByte,"utf-8");
        System.out.println("请求数据："+inData);


        System.out.println("out");
        ByteArrayInputStream in = new ByteArrayInputStream("{\"isOk\":true,\"message\":\"响应数据\"}".getBytes());
        OutputStream out = sock.getOutputStream();

        int len ;
        byte[] buffer = new byte[1024];
        while((len = in.read(buffer)) > 0){

            out.write(buffer,0,len);
            out.flush();

        }
        out.close();
        in.close();
        inputStream.close();
        sock.close();
        server.close();
    }
}
