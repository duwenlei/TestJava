package day.du20170727lamadar;

/**
 * Created by duwenlei on 17-7-27.
 */
public class Java8Tester {
    interface MathOperation {
        int operation(int a, int b);
    }

    private int operation(int a, int b, MathOperation mathOperation) {
        return mathOperation.operation(a, b);
    }

    public static void main(String[] args) {
        MathOperation addOperation = (int a, int b) -> {
            return a + b;
        };
        MathOperation subtraction = (a, b) -> a - b;
        MathOperation multiplication = (int a, int b) -> {
            return a * b;
        };

        Java8Tester tester = new Java8Tester();
        System.out.println(tester.operation(3, 4, multiplication));

    }
}


