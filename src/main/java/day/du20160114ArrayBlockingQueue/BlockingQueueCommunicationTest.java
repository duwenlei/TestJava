package day.du20160114ArrayBlockingQueue;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * 可阻塞的队列
 * BlockingQueue
 * 此方式不能实现线程互斥
 * @author
 *
 */
public class BlockingQueueCommunicationTest {
	public static void main(String[] args) {
		final BlockingQueue<Integer> queue = new ArrayBlockingQueue<Integer>(3);
		new Thread(new Runnable() {
			public void run() {
				while (true) {
					try {
						Thread.sleep(new Random().nextInt(1000));
						System.out.println("线程"
								+ Thread.currentThread().getName() + "准备增加");
						queue.put(1);
						System.out.println("线程"
								+ Thread.currentThread().getName() + "已增加，队列有"
								+ queue.size() + "个");
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();

		new Thread(new Runnable() {
			public void run() {
				while (true) {
					try {
						Thread.sleep(new Random().nextInt(1000));
						System.out.println("线程"
								+ Thread.currentThread().getName() + "准备取走");
						queue.take();
						System.out.println("线程"
								+ Thread.currentThread().getName() + "已取走，队列剩余"
								+ queue.size() + "个");
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();

	}
}
