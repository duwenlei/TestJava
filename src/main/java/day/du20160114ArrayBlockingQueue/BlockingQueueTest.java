package day.du20160114ArrayBlockingQueue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * 使用BlockingQueue实现主子线程互相打印
 * @author duwenlei
 *
 */
public class BlockingQueueTest {

	public static void main(String[] args) {
		final Business business = new Business();		
		new Thread(new Runnable() {
			public void run() {
				while(true){
					business.sub();
				}
			}
		}).start();
		
		while(true){
			business.main();
		}
	}
	
	static class Business{
		private BlockingQueue<Integer> queue1 = new ArrayBlockingQueue<Integer>(1);
		private BlockingQueue<Integer> queue2 = new ArrayBlockingQueue<Integer>(1);
		{
			try {
				queue2.put(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		public void sub(){
			try {
				queue1.put(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			for (int i = 1; i < 11; i++) {
				System.out.println("sub print "+ i);
			}
			try {
				queue2.take();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		public void main(){
			try {
				queue2.put(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			for (int i = 1; i < 101; i++) {
				System.out.println("main print "+i);
			}
			try {
				queue1.take();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
}
