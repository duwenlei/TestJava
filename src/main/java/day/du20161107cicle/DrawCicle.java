package day.du20161107cicle;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;

/**
 * @author duwenlei 画圆
 * 
 *         oval：椭圆
 * 
 *         Polygon：多边形
 * 
 *         rectangle:矩形
 */
public class DrawCicle {

	public static void main(String[] args) throws Exception {
		BufferedImage image = new BufferedImage(410, 510, BufferedImage.TYPE_INT_RGB);
		Graphics2D grap = (Graphics2D) image.getGraphics();
		// grap.setColor(Color.RED);
		// grap.drawString("hello java grap", 10, 20);
		//
		// int [ ] x = {20, 35, 50, 65, 80, 95};
		// int [ ] y = {60, 105, 105, 110, 95, 95};
		//
		// grap.setColor(Color.blue);
		// grap.drawPolygon(x, y, 6);

		grap.setColor(Color.RED);
		grap.drawOval(10, 10, 200, 200);

		ImageOutputStream out = new FileImageOutputStream(new File("D:/test.jpg"));
		ImageIO.write(image, "jpg", out);
		drawEmptyCircle();
		System.out.println("draw over");
	}

	/**
	 * @throws Exception
	 *             使用graphics画圆
	 */
	public static void drawEmptyCircle() throws Exception {
		BufferedImage image = new BufferedImage(420, 500, BufferedImage.TYPE_INT_RGB);
		Graphics2D grap = (Graphics2D) image.getGraphics();

		grap.setColor(Color.RED);
		grap.setStroke(new BasicStroke(3f));
		grap.drawOval(0, 0, 200, 200);
		
		FileImageOutputStream out = new FileImageOutputStream(new File("D:/circle.jpg"));
		ImageIO.write(image, "jpg", out);
	}

	/**
	 * 旋转文字
	 */
	public void roateWord() {

	}

}
