package day.du20160106Threadbuyticket;

/**
 * 买票
 * @author duwenlei
 *
 */
public class BuyTicketTest {

	public static void main(String[] args) {
		final Ticket ticket = new Ticket();
		for (int i = 0; i < 101; i++) {
			new Thread(new Runnable() {
				public void run() {
					ticket.decrease();
				}
			}).start();
		}
	}
}

class Ticket {
	private int ticketPollCount = 100;	//总票数
	public synchronized void decrease() {
		if (ticketPollCount == 0) {
			System.out.println("没有足够的票！");
			return;
		}
		ticketPollCount--;
		System.out.println(ticketPollCount);
	}
}
