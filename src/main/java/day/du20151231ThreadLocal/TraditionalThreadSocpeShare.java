package day.du20151231ThreadLocal;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * ThreadLocal 实现线程范围内变量共享
 * 
 * @author duwenlei
 *
 */
public class TraditionalThreadSocpeShare {
	private static Map<Thread, Integer> threadLocal = new HashMap<Thread, Integer>();
	public static void main(String[] args) {
		for (int i = 0; i < 2; i++) {
			new Thread(new Runnable() {
				public void run() {
					int data = new Random().nextInt();
					threadLocal.put(Thread.currentThread(), data);
					System.out.println(Thread.currentThread().getName()
							+ " data value " + data);
					new A().get();
					new B().get();
				}

			}).start();
		}
	}

	static class A {
		public void get() {
			int data = threadLocal.get(Thread.currentThread());
			System.out.println("A from " + Thread.currentThread().getName()
					+ " data value " + data);
		}
	}

	static class B {
		public void get() {
			int data = threadLocal.get(Thread.currentThread());
			System.out.println("B from " + Thread.currentThread().getName()
					+ " data value " + data);
		}
	}
}
