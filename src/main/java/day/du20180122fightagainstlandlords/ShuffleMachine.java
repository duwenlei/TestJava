package day.du20180122fightagainstlandlords;

import java.util.*;

/**
 * Created by duwenlei on 2018-1-22.
 */
public class ShuffleMachine {
    /**
     * 红
     * 黑
     * 花
     * 方
     * 大王
     * 小王
     */
    private static final String[] pokers = {
            "红1", "红2", "红3", "红4", "红5", "红6", "红7", "红8", "红9", "红10", "红11", "红12", "红13",
            "黑1", "黑2", "黑3", "黑4", "黑5", "黑6", "黑7", "黑8", "黑9", "黑10", "黑11", "黑12", "黑13",
            "花1", "花2", "花3", "花4", "花5", "花6", "花7", "花8", "花9", "花10", "花11", "花12", "花13",
            "方1", "方2", "方3", "方4", "方5", "方6", "方7", "方8", "方9", "方10", "方11", "方12", "方13",
            "14", "15",};

    /**
     * 洗牌
     * 正常情况下每次洗三遍，换了 3 * 54 = 162次，大约是150次上下
     * 伪随机
     * @return
     */
    public static String[] shuffle() {
        String[] pokers = ShuffleMachine.pokers;
        Random random = new Random(System.currentTimeMillis());

        for (int i = 0; i < 250; i++) {
            int t1 = random.nextInt(54);
            int t2 = random.nextInt(54);

            if (t1 == t2){
                System.out.println("t1 == t2");
                t1 = random.nextInt(54);
                t2 = random.nextInt(54);
            }

            String poker1 = pokers[t1];
            pokers[t1] = pokers[t2];
            pokers[t2] = poker1;
        }

        return pokers;
    }

    /**
     * 发牌
     *
     * @return
     */
    public static Map<String, List<String>> deal(String[] pokers) {
        List<String> player1 = new ArrayList<>(17);
        List<String> player2 = new ArrayList<>(17);
        List<String> player3 = new ArrayList<>(17);
        List<String> landlord = new ArrayList<>(3);
        int index = 0;
        for (int i = 0; i < 17; i++) {
            player1.add(pokers[index]);
            index++;
            player2.add(pokers[index]);
            index++;
            player3.add(pokers[index]);
            index++;
        }
        landlord.add(pokers[index]);
        index++;
        landlord.add(pokers[index]);
        index++;
        landlord.add(pokers[index]);

        Map<String, List<String>> result = new HashMap(4);
        result.put("player1", player1);
        result.put("player2", player2);
        result.put("player3", player3);
        result.put("landlord", landlord);
        return result;
    }

    public static void main(String[] args) {
        //洗牌
        String[] p = shuffle();
        //发牌
        Map<String, List<String>> result = deal(p);
        List<String> player1 = result.get("player1");
        List<String> player2 = result.get("player2");
        List<String> player3 = result.get("player3");
        List<String> landlord = result.get("landlord");
        for (String poker : player1) {
            System.out.print(poker + ",");
        }
        System.out.println();
        for (String poker : player2) {
            System.out.print(poker + ",");
        }
        System.out.println();
        for (String poker : player3) {
            System.out.print(poker + ",");
        }
        System.out.println();
        for (String poker : landlord) {
            System.out.print(poker + ",");
        }
    }
}
