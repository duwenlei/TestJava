package day.du20151230ThreadSynch;

/**
 * 线程通信
 * 
 * @author duwenlei
 *
 */
public class TraditionalThreadCommunication01 {
	public static void main(String[] args) {

		new Thread(new Runnable() {
			public void run() {
				for (int i = 1; i <= 50; i++) {
					synchronized (TraditionalThreadCommunication01.class) {
						for (int j = 1; j <= 10; j++) {
							System.out.println("sub thread " + j + " 次数" + i);
						}
					}
				}
			}
		}).start();

		for (int i = 1; i <= 50; i++) {
			synchronized (TraditionalThreadCommunication01.class) {
				for (int j = 1; j < 100; j++) {
					System.out.println("mian thread " + j + "  count " + i);
				}
			}
		}
	}
}
