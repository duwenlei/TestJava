package day.du20151230ThreadSynch;

/**
 * 线程通信
 * 
 * @author duwenlei
 *
 */
public class TraditionalThreadCommunication02 {
	
	
	public static void main(String[] args) {
		final Business b = new Business();
		new Thread(new Runnable() {
			public void run() {
				for (int i = 1; i <= 50; i++) {
					b.sub(i);
				}
			}
		}).start();

		for (int i = 1; i <= 50; i++) {
			b.main(i);
		}
	}
}
class Business{
	private boolean flag = true;
	public synchronized void sub(int i){
		if(!flag){
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		for (int j = 1; j <= 10; j++) {
			System.out.println("sub thread " + j + " 次数" + i);
		}
		flag = false;
		this.notify();
	}
	public synchronized void main(int i){
		if(flag){
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		for (int j = 1; j < 100; j++) {
			System.out.println("mian thread " + j + "  count " + i);
		}
		
		flag = true;
		this.notify();
	}
}
