package day.du20151230ThreadSynch;

/**
 * 子线程循环10，主线程循环100次，如此反复50次，主子线程互不打扰
 * @author duwenlei
 *
 */
public class TraditionalThreadTest {
	
	public static void main(String[] args) {	//主线程
		final BusinessTest b = new BusinessTest();
		new Thread(new Runnable() {	//子线程
			public void run() {
				for (int i = 1; i <= 50; i++) {
					b.sub(i);
				}
			}
		}).start();
		
		for (int i = 1; i <= 50; i++) {
			b.main(i);
		}
		
	}

}
class BusinessTest{
	 private boolean flag = true;
	 public synchronized void sub(int i){
		 while (!flag) {	//true 執行
			try {
				this.wait();	//false是等待
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		 for (int j = 1; j <= 10; j++) {
			System.out.println("子线程 ： "+ j + ",第"+ i +"次打印");
		}
		 flag = false;
		 this.notify();	//通知下一个线程
		 System.out.println();
	 }
	 public synchronized void main(int i){
		 while (flag) {	//false 執行
			try {
				this.wait();	//true 等待
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		 for (int j = 1; j <= 10; j++) {
			System.out.println("主线程 ： "+ j + ",第"+ i +"次打印");
		}
		 flag = true;
		 this.notify();	//通知下一个线程
		 System.out.println();
	 }
}