package day.du20151214rabbit;

public class Main {
	public static void main(String[] args) {
		
//		int rabbitNum = 0;
//		for (int i = 1; i <= 10; i++) {
//			rabbitNum = getMonthNum(i);
//			System.out.println("第"+i+"月,兔子的数量为 : "+rabbitNum);
//		}
		getMonthNum2(10);
		
	}
	
	/**
	 * 递归解决
	 * @param month
	 * @return
	 */
	@SuppressWarnings("unused")
	private static int getMonthNum(int month){
		if(month ==1 || month ==2) {
			return 1;
		}
		return getMonthNum(month-1)+getMonthNum(month-2);
	}
	
	/**
	 * 循环解决
	 * @param month
	 */
	private static void getMonthNum2(int month){
		int n1 = 1,n2 = 1;
		int n3 = 0;
		System.out.println("第1月,兔子数量为 : 1");
		System.out.println("第2月,兔子数量为 : 1");
		for (int i = 3; i <= month; i++) {
			n3 = n1+n2;
			n1 = n2;
			n2 = n3;
			System.out.println("第"+i+"月,兔子数量为 : "+n3);
		}
	}
}
