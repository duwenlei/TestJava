package day.du20151217strutil;

public class StringUtils extends org.apache.commons.lang3.StringUtils{
	
	public static void main(String[] args) {
		//空串判断
		System.out.println(StringUtils.isEmpty(""));
		System.out.println(StringUtils.isNotEmpty(""));
		
		//
		System.out.println(StringUtils.isBlank(" "));
		System.out.println(StringUtils.isNotBlank(" "));
		
		//前后替换
		System.out.println(StringUtils.strip("124561278912", "12"));
	}
}
