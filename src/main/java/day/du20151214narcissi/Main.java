package day.du20151214narcissi;

public class Main {
	public static void main(String[] args) {
		
		for (int i = 100; i < 1000; i++) {
			int firstNumber = i%10;	//个位
			int secondNumber = (i/10)%10;	//十位
			int thirdNumber = (i/100)%10;	//百位
			
			int sum = (int) (Math.pow(firstNumber,3)+Math.pow(secondNumber,3)+Math.pow(thirdNumber,3));
			
			if(sum == i){
				System.out.print(i+"\t");
			}
		}
	}
}
