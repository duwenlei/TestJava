package day.memory.heapoutmemory;

import java.util.ArrayList;
import java.util.List;

/**
 * heap out memory 
 * @author duwenlei
 *
 */
public class HeapOutMemoryTest1 {

	public static void main(String[] args) {
		List<Object> testHeapMemory = new ArrayList<Object>();
		try {
			while (true) {
				Object e = new Object();
				e = 1;
				testHeapMemory.add(e);
			}
		} catch (OutOfMemoryError e) {
			System.out.println(e.getLocalizedMessage());
		}
	}

}
