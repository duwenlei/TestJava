package day.du20160111CyclicBarrier;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CyclicBarrierTest {

	public static void main(String[] args) {
		ExecutorService newCachedThreadPool = Executors.newCachedThreadPool();
		final CyclicBarrier cyclicBarrier = new CyclicBarrier(3);
		for (int i = 0; i < 3; i++) {
			Runnable runnable = new Runnable() {
				public void run() {
					try {
						Thread.sleep(new Random().nextInt(10000));
						System.out.println("线程"+Thread.currentThread().getName()+"即将到达1，当前已有"+(cyclicBarrier.getNumberWaiting()+1)+"已经到达"+(cyclicBarrier.getNumberWaiting()==2?"都到齐了，继续走":"正在等待"));
						cyclicBarrier.await();
						
						Thread.sleep(new Random().nextInt(10000));
						System.out.println("线程"+Thread.currentThread().getName()+"即将到达2, 当前已有"+(cyclicBarrier.getNumberWaiting()+1)+"已经到达"+(cyclicBarrier.getNumberWaiting()==2?"都到齐了,继续走":"正在等待"));
						cyclicBarrier.await();
						
						Thread.sleep(new Random().nextInt(10000));
						System.out.println("线程"+Thread.currentThread().getName()+"即将到达3, 当前已有"+(cyclicBarrier.getNumberWaiting()+1)+"已经到达"+(cyclicBarrier.getNumberWaiting()==2?"都到齐了,继续走":"正在等待"));
						cyclicBarrier.await();
					} catch (InterruptedException e) {
						e.printStackTrace();
					} catch (BrokenBarrierException e) {
						e.printStackTrace();
					}
				}
			};
			newCachedThreadPool.execute(runnable);
		}
		newCachedThreadPool.shutdown(); //执行完成后关闭线程池
	}
}
