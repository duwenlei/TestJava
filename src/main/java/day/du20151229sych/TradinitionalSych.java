package day.du20151229sych;

/**
 * 线程互斥
 * @author duwenlei
 *
 */
public class TradinitionalSych {
	
	public static void main(String[] args) {
		
		new TradinitionalSych().init();
		
	}
	
	private void init() {
		final Outputer out = new Outputer();
		new Thread(new Runnable() {
			public void run() {
				while (true) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					out.output("duwenlei");	
				}
			}
		}).start();
		
		new Thread(new Runnable() {
			public void run() {
				while (true) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					out.output("shenjing");	
				}
			}
		}).start();
	}
	
	/**
	 * 线程互斥同步，条件是必须有一个统一的参照
	 *	name不可以，
	 *	this：只有在相同Outputer对象时能行的痛
	 *	Outputer.class类的字节码 在java中也是一个对象，可以 
	 * @author duwenlei
	 *
	 */
	class Outputer{
		public void output(String name) {
			int len = name.length();
			synchronized (Outputer.class) {
				for (int i = 0; i < len; i++) {
					System.out.print(name.charAt(i));
				}
				System.out.println();	
			}
		}
	}

}
