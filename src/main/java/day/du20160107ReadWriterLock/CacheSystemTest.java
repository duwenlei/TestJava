package day.du20160107ReadWriterLock;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class CacheSystemTest {

	private Map<String, Object> cacheMap = new HashMap<String, Object>();
	
	private ReadWriteLock rwl = new ReentrantReadWriteLock();
	
	/**
	 * 为了防止在多线程情景下数据安全问题，需要线程互斥，实现方式是用锁
	 * @param key
	 * @return
	 */
	public Object get(String key){
		rwl.readLock().lock();	//任何一个线程进来后，第一时间加上读锁
		Object obj = null; 
		try{
			obj = cacheMap.get(key);
			if(obj == null){
				rwl.readLock().unlock();	//在赋值前关闭读锁，并在此检查对象
				if(obj == null){
					rwl.writeLock().lock();	//打开一个写锁
					try{
						obj = "read write lock";
					}finally{
						rwl.writeLock().unlock();	//关闭写锁
						rwl.readLock().lock();	//恢复正常读锁
					}
				}
			}
		}finally{
			rwl.readLock().unlock();
		}
		return obj;
	}
}
