package day.du20151228Timer;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 定时器
 * @author duwenlei
 *
 */
public class TraditionalTimerTest {

	public static void main(String[] args) {
		
		new Timer().schedule(new TimerTask() {
			
			@Override
			public void run() {
				System.out.println("炸弹爆炸");
			}
		}, 10000);
		
	}
}
