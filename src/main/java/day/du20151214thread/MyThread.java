package day.du20151214thread;

public class MyThread extends Thread{

	private static int temp = 0;
	
	@Override
	public void run() {
		for (int i = 0; i < 100; i++) {
			try {
				MyThread.sleep(2000);
				temp = i;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public static int getTemp() {
		return temp;
	}

	public static void setTemp(int temp) {
		MyThread.temp = temp;
	}
}
