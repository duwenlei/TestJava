package day.du20151215properties;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

public class Main {
	/**
	 * 31天的月份 
	 */
	private static final List<Integer> singleMonList = new ArrayList<Integer>();
	
	static{
		singleMonList.add(0);
		singleMonList.add(2);
		singleMonList.add(4);
		singleMonList.add(6);
		singleMonList.add(7);
		singleMonList.add(9);
		singleMonList.add(11);
	}
	
	public static void calendarYear(int year){
			Calendar c = Calendar.getInstance();
			c.set(Calendar.YEAR, year);
			System.out.println("-----------------------------"+year+"年start------------------------------");
			for (int i = 0; i < 12; i++) {
				System.out.println();
				System.out.println(year + "年" + (i + 1) + "月");
				System.out.println();
				System.out.println("日\t" + "一\t" + "二\t" + "三\t" + "四\t"+ "五\t" + "六");
				c.set(Calendar.MONTH, i);
				c.set(Calendar.DATE, 1);
				int week = c.get(Calendar.DAY_OF_WEEK);
				int weekTemp = week - 1;
				int days = getMonthOfDays(year, i); // 获取天数
				// 天数打印
				for (int j = 1; j <= days; j++) {
					if (j == 1){
						getBlank(weekTemp); // 打印空格
					}
					if (weekTemp == 7) {	//换行
						System.out.println();
						if (j < 10) {
							System.out.print(" " + j + "\t");
						} else {
							System.out.print(j + "\t");
						}
						weekTemp = 1;
					} else {
						if (j < 10) {
							System.out.print(" " + j + "\t");
						} else {
							System.out.print(j + "\t");
						}
						weekTemp++;
					}
				}
				System.out.println();
				System.out.println();
				System.out.println();
			}
			System.out.println("-----------------------------"+year+"年end------------------------------");
	}

	private static void getBlank(int blankNum) {
		for (int i = 0; i < blankNum; i++) {
			System.out.print(" \t");
		}
	}

	private static int getMonthOfDays(int year, int month) {
		int days = 0;
		if (singleMonList.contains(month)) {
			days = 31;
		} else {
			if (month == 1) {
				if (((year % 100 != 0) && (year % 4 == 0))
						|| ((year % 100 == 0) && (year % 400 == 0))) {
					days = 29;
				} else {
					days = 28;
				}
			} else {
				days = 30;
			}
		}
		return days;
	}
	
	private static boolean checkYear(int year){
		if(year>Long.MAX_VALUE){
			return false;
		}
		if(year < Long.MIN_VALUE){
			return false;
		}
		return true;
	}
	
	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		
		while(true){
			System.out.print("请输入年份 (1: 退出程序): ");
			Scanner sc = new Scanner(System.in);
			Integer year = sc.nextInt();
			if(!checkYear(year)) {
				continue;
			}
			if(year==1) System.exit(0);
			calendarYear(year);
		}
	}
}
