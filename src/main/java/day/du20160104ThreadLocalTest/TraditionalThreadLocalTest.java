package day.du20160104ThreadLocalTest;

import java.util.Random;

/**
 * ThreadLocal的使用
 * @author Test
 *
 */
public class TraditionalThreadLocalTest {
	public static void main(String[] args) {
		new Thread(new Runnable() {
			public void run() {
				Person p = Person.getInstance_t();
				p.setAge(new Random().nextInt());
			}
		}).start();
	}
}
class Person{
	private Person(){
	}
	
	//单例实现 1 饿汉实现方式
	private static Person person = new Person();	//单例，保证程序中只存在一个person对象		
	public static Person getInstance(){
		return person;
	}
	
	//单例实现 2 懒汉模式实现
	private static Person person_ = null;
	public static synchronized Person getInstance_(){	//synchronized:避免两个线程同时进入代码，而出现覆盖对象的情况
		if(person_==null){
			person_ = new Person();
			return person_;
		}else{
			return person_;
		}
	}

	// 单例模式，3 登记模式
	/*
	 *这个模式主要特点是，每一个线程，都拥有自己的对象，需要时只拿自己生产的对象
	 * 
	 */
	private static ThreadLocal<Person> threadlocal = new ThreadLocal<Person>();
	public static Person getInstance_t(){
		 Person p = threadlocal.get();
		 if(p == null){
			Person ps = new Person();
			threadlocal.set(ps);
			return ps;
		 }
		 return p;
	}
	
	
	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	private int age;
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
}
