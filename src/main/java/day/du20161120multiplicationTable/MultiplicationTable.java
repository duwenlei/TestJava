package day.du20161120multiplicationTable;

/**
 * @author duwenlei 九九加法表 九九乘法表
 */
public class MultiplicationTable {

	/**
	 * 乘法
	 */
	public static void addition() {
		System.out.println("***********************************九九加法表**************************************");
		System.out.println("+\t0\t1\t2\t3\t4\t5\t6\t7\t8\t9");
		for (int i = 0; i < 10; i++) {
			System.out.print(i + "\t");
			for (int j = 0; j < 10; j++) {
				System.out.print(i + j + "\t");
			}
			System.out.println();
		}
	}

	/**
	 * 加法
	 */
	public static void multiplication() {
		System.out.println("***********************************九九乘法表**************************************");
		System.out.println("*\t0\t1\t2\t3\t4\t5\t6\t7\t8\t9");
		for (int i = 0; i < 10; i++) {
			System.out.print(i + "\t");
			for (int j = 0; j < 10; j++) {
				System.out.print(i * j + "\t");
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		multiplication();
		System.out.println();
		System.out.println();
		addition();
	}

}
