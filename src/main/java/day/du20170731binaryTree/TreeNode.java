package day.du20170731binaryTree;

/**
 * Created by duwenlei on 17-7-31.
 */
public class TreeNode {
    public int value;

    public TreeNode(int value){
        this.value = value;
    }

    TreeNode left;
    TreeNode right;
}
