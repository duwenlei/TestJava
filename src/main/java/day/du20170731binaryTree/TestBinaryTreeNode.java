package day.du20170731binaryTree;

import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by duwenlei on 17-7-31.
 * <p>
 * https://github.com/yuzhangcmu/LeetCode/blob/master/tree/TreeDemo.java
 */
public class TestBinaryTreeNode {

    private static TreeNode node = new TreeNode(1);

    @Before
    public void setUp() {
        TreeNode r1 = new TreeNode(2);
        TreeNode r2 = new TreeNode(3);
        TreeNode r3 = new TreeNode(4);
        TreeNode r4 = new TreeNode(5);
        TreeNode r5 = new TreeNode(6);
        TreeNode r6 = new TreeNode(7);
        TreeNode r7 = new TreeNode(8);
        TreeNode r8 = new TreeNode(9);

        node.left = r1;
        node.right = r2;
        r1.left = r3;
        r1.right = r4;
        r2.left = r5;
        r2.right = r6;
        r3.left = r7;
        r3.right = r8;
    }


    /**
     * @param node 二叉树对象，迭代
     * @apiNote 求二叉树的节点个数
     */
    public static int getNodeNum(TreeNode node) {
        if (node == null) {
            return 0;
        }
        return getNodeNum(node.left) + getNodeNum(node.right) + 1;
    }

    /**
     * @param node 二叉树对象
     * @apiNote 求二叉树深度 迭代
     */
    public static int getNodeDepth(TreeNode node) {
        if (node == null)
            return 0;

        return Math.max(getNodeDepth(node.left), getNodeDepth(node.right)) + 1;
    }

    /**
     * 前排序
     */
    public static void preOrder(TreeNode node) {
        if (node == null) {
            return;
        }

        System.out.print(node.value + "\t");
        preOrder(node.left);
        preOrder(node.right);
    }

    /**
     * 中遍历
     */
    public static void inOrder(TreeNode node) {
        if (node == null)
            return;

        inOrder(node.left);
        System.out.print(node.value + "\t");
        inOrder(node.right);
    }

    public static void postOrder(TreeNode node) {
        if (node == null)
            return;

        postOrder(node.left);
        postOrder(node.right);
        System.out.print(node.value + "\t");
    }

    /**
     * 分层便利二叉树
     * <p>
     * 从上到下，从左到右
     * <p>
     * offer == add
     * <p>
     * poll == get
     */
    public static void levelTraveral(TreeNode node) {
        if (node == null)
            return;

        Queue<TreeNode> root = new LinkedList<TreeNode>();
        root.offer(node);

        while (!root.isEmpty()) {
            TreeNode currentNode = root.poll();

            System.out.print(currentNode.value + "\t");
            if (currentNode.left != null) {
                root.offer(currentNode.left);
            }
            if (currentNode.right != null) {
                root.offer(currentNode.right);
            }
        }
    }


    /**
     * 题目要求：将二叉查找树转换成排序的双向链表，不能创建新节点，只调整指针。
     * 查找树的结点定义如下：
     * 既然是树，其定义本身就是递归的，自然用递归算法处理就很容易。将根结点的左子树和右子树转换为有序的双向链表，
     * 然后根节点的left指针指向左子树结果的最后一个结点，同时左子树最后一个结点的right指针指向根节点；
     * 根节点的right指针指向右子树结果的第一个结点，
     * 同时右子树第一个结点的left指针指向根节点。
     */
    public static TreeNode[] convertBT2DLL(TreeNode node) {
        TreeNode[] ret = new TreeNode[2];
        ret[0] = null;
        ret[1] = null;

        if (node.left != null) {
            TreeNode[] left = convertBT2DLL(node.left);
            left[1].right = node;
            node.left = left[1];
            ret[0] = left[0];
        } else {
            ret[0] = node;
        }
        if (node.right != null) {
            TreeNode[] right = convertBT2DLL(node.right);
            right[0].left = node;
            node.right = right[1];
            ret[1] = right[0];
        } else {
            ret[1] = node;
        }


        return ret;
    }

    public static int getNodeNumKLevel(TreeNode node, int k) {
        if (node == null || k == 0)
            return 0;
        if (k == 1)
            return 1;

        return getNodeNumKLevel(node.left, k - 1) + getNodeNumKLevel(node.right, k - 1);
    }

    @Test
    public void test() {
//        System.out.println("binary tree node number is " + getNodeNum(node));
//        System.out.println("binary tree node depth is " + getNodeDepth(node) + " floor");
        System.out.println(getNodeNumKLevel(node,3));
    }
}
