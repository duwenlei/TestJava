package day.du20170531swift;

import org.javaswift.joss.client.factory.AccountConfig;
import org.javaswift.joss.client.factory.AccountFactory;
import org.javaswift.joss.client.factory.AuthenticationMethod;
import org.javaswift.joss.model.Account;
import org.javaswift.joss.model.Container;
import org.javaswift.joss.model.StoredObject;

import java.io.File;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by duwenlei on 17-5-31.
 */
public class TestSwift {
    public static void main(String[] args) {
        String username = "E382AA6C7C4170C277E483EA0E6E01CF:newtest";
        String password = "QjRDNjlCMEM1M0UwRkRBRTY1QkUxRUZFNDRFNzAzREI1MTUxOUExOQ";
        String authUrl  = "https://122.228.11.108/auth/v1.0";//https://122.228.11.108/auth/v1.0

        AccountConfig config = new AccountConfig();
        config.setDisableSslValidation(true);
        config.setUsername(username);
        config.setPassword(password);
        config.setAuthUrl(authUrl);
        config.setAuthenticationMethod(AuthenticationMethod.BASIC);
        Account account = new AccountFactory(config).createAccount();
        System.out.println(account.getObjectCount());

        //Container container = account.getContainer("my-new-container");

        //StoredObject object = container.getObject("joss-0.9.14.jar");
        //object.uploadObject(new File("/home/duwenlei/Downloads/joss-0.9.14.jar"));

//        StoredObject object = container.getObject("joss-0.9.14.jar");
//        Map<String, Object> metadata = new TreeMap<String, Object>();
//        metadata.put("key", "value");
//        object.setMetadata(metadata);

//        Collection<Container> containers = account.list();
//        for (Container currentContainer : containers) {
//            System.out.println(currentContainer.getName());
//        }
//        Container container = account.getContainer("my-new-container");
//        Collection<StoredObject> objects = container.list();
//        for (StoredObject currentObject : objects) {
//            System.out.println(currentObject.getName());
//        }

//        Container container = account.getContainer("my-new-container");
//        StoredObject object = container.getObject("joss-0.9.14.jar");
//        Map<String, Object> returnedMetadata = object.getMetadata();
//        for (String name : returnedMetadata.keySet()) {
//            System.out.println("META / "+name+": "+returnedMetadata.get(name));
//        }

        Container container = account.getContainer("my-new-container");
        StoredObject object = container.getObject("joss-0.9.14.jar");
        object.downloadObject(new File("/home/duwenlei/Downloads/joss-0.9.14-swift.jar"));
    }
}
