package day.du20160113CountdownLatch;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 倒计时 countdownlatch
 * @author duwenlei
 *
 */
public class CountDownLatchTest {

	public static void main(String[] args) {
		ExecutorService service = Executors.newCachedThreadPool();
		final CountDownLatch subLatch = new CountDownLatch(1);	//运动员的
		final CountDownLatch mainLatch = new CountDownLatch(3);	//裁判的计数器
		for (int i = 0; i < 3; i++) {
			Runnable runnable = new Runnable(){
				public void run() {
					try {
						System.out.println("线程"+Thread.currentThread().getName()+"准备待命");
						subLatch.await();	//运动员等待裁判发出口令
						System.out.println("线程"+Thread.currentThread().getName()+"已经收到命令");
						
						Thread.sleep(new Random().nextInt(10000));
						System.out.println("线程"+Thread.currentThread().getName()+"线程已完成");	//已经跑完
						mainLatch.countDown();		//通知裁判
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			};
			service.execute(runnable);
		}
		
		try {
			Thread.sleep(new Random().nextInt(10000));
			System.out.println("线程"+Thread.currentThread().getName()+"即将发送命令");	//发出口令，3个线程开始跑步
			subLatch.countDown();	//计数器减一
			
			System.out.println("线程"+Thread.currentThread().getName()+"正在等待结果");	//口令发出后等待结果
			mainLatch.await();	//等待所有跑完
			
			System.out.println("线程"+Thread.currentThread().getName()+"已收到所有响应");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		service.shutdown();
	}
}
