package day.du20160113Exchanger;

import java.util.Random;
import java.util.concurrent.Exchanger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExchangerTest {
	public static void main(String[] args) {
		ExecutorService service = Executors.newCachedThreadPool();
		final Exchanger<String> exchanger = new Exchanger<String>();
		service.execute(new Runnable() {
			public void run() {
				try {
					String data1 = "duwenlei";
					System.out.println("线程"+Thread.currentThread().getName()+"要换的数据为"+data1);
					System.out.println("线程"+Thread.currentThread().getName()+"等待对方");
					Thread.sleep(new Random().nextInt(10000));
					String data2 = exchanger.exchange(data1);	//开始互换数据
					System.out.println("线程"+Thread.currentThread().getName()+"互换后的数据为"+data2);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		service.execute(new Runnable() {
			public void run() {
				try {
					String data1 = "shenjing";
					System.out.println("线程"+Thread.currentThread().getName()+"要换的数据为"+data1 );
					System.out.println("线程"+Thread.currentThread().getName()+"等待对方");
					Thread.sleep(new Random().nextInt(10000));
					String data2 = exchanger.exchange(data1);
					System.out.println("线程"+Thread.currentThread().getName()+"互换后的数据为"+data2);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		service.shutdown();
	}
}
