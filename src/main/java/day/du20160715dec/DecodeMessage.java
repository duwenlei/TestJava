package day.du20160715dec;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

@SuppressWarnings("unused")
public class DecodeMessage {
	static Cipher cipher;
	static final String KEY_ALGORITHM = "AES";
	static final String CIPHER_ALGORITHM_ECB = "AES/ECB/PKCS5Padding";
	static final String CIPHER_ALGORITHM_CBC = "AES/CBC/PKCS5Padding";
	static final String CIPHER_ALGORITHM_CBC_NoPadding = "AES/CBC/NoPadding";
	static SecretKey secretKey;

	public static void jiemi() throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException,
			UnsupportedEncodingException {
		String message = "qTduhP_xAoYfabkzRzZJDumfdbmEOkYIMfkBPPDRYVGUciSK7liPa6TSEfUutw8LTdsoNCkkE_6figyd8-ByZ9leo9zFPcj8NqgWCI3i0bSR-gbfhMpI86FgwgzhfyON4mf7MpbpYH-usalWBqZ_ncx62qY4bWiqKIRTcgYYAZaJ3vYAyNF4nHeP7IUJjx9GlQWOwfH51qRUrI2_TEJLarxeUTauhK_HxR1ho-BH9Zc";
		String newmesage = message.replaceAll("_", "/").replaceAll("-", "+") + "=";
		byte[] result = Base64.decodeBase64(newmesage);
		SecretKeySpec aesKey = new SecretKeySpec(
				"a12d765e3319adc129c9b822d5c1eb84".getBytes(), "AES");
		Cipher aesCipher = Cipher.getInstance("AES");
		aesCipher.init(Cipher.ENCRYPT_MODE, aesKey); //
		byte[] plaintext = "Hello!".getBytes();
		byte[] ciphertext = aesCipher.doFinal(plaintext);
		aesCipher.init(Cipher.DECRYPT_MODE, aesKey);
		byte[] decrypted = aesCipher.doFinal(result);
		System.out.println(new String(decrypted, "utf-8"));
	}

	public static void main(String[] args) {
		byte[] sec = HexStringToBinary("a12d765e3319adc129c9b822d5c1eb84");
		SecretKeySpec aesKey = new SecretKeySpec(sec,0,sec.length, "AES");
		String message = "qTduhP_xAoYfabkzRzZJDumfdbmEOkYIMfkBPPDRYVGUciSK7liPa6TSEfUutw8LTdsoNCkkE_6figyd8-ByZ9leo9zFPcj8NqgWCI3i0bSR-gbfhMpI86FgwgzhfyON4mf7MpbpYH-usalWBqZ_ncx62qY4bWiqKIRTcgYYAZaJ3vYAyNF4nHeP7IUJjx9GlQWOwfH51qRUrI2_TEJLarxeUTauhK_HxR1ho-BH9Zc";
		String newmesage = message.replaceAll("_", "/").replaceAll("-", "+");
		while(true){
			if(newmesage.length()%4 != 0){
				newmesage+="=";
			}else{
				break;
			}
		}
		System.out.println(newmesage);
		byte[] sss = Base64.decodeBase64(newmesage);
		
		Cipher cipher;
		try {
			cipher = Cipher.getInstance(aesKey.getAlgorithm());
			cipher.init(Cipher.DECRYPT_MODE, aesKey);
			System.out.println(new String(cipher.doFinal(Base64.decodeBase64(newmesage))));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//	    test();
	}
	public static void test() {
		String originString = "duwenlei";
		byte[] orignData = "duwenlei".getBytes();
		String secret = "0d51a6fca86d245ef539033074bc1ad5";	//十六进制
		

		byte[] sec = new BigInteger(secret,16).toByteArray();
		SecretKeySpec aesKey = new SecretKeySpec(sec,0,sec.length, "AES");
		
		byte[] encrypted = null;
		try{
			Cipher cipher = Cipher.getInstance(aesKey.getAlgorithm());
			cipher.init(Cipher.ENCRYPT_MODE, aesKey);
			encrypted = cipher.doFinal(orignData);	
			System.out.println(Base64.encodeBase64String(encrypted));
		}catch(Exception e){
			e.printStackTrace();
		}
		
		try{
			Cipher cipher = Cipher.getInstance(aesKey.getAlgorithm());
			cipher.init(Cipher.DECRYPT_MODE, aesKey);
			byte[] decrypted = cipher.doFinal(encrypted);
			System.out.println(new String(decrypted));
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	private static String hexStr =  "0123456789ABCDEF";
	 /** 
     *  
     * @param hexString 
     * @return 将十六进制转换为字节数组 
     */  
    public static byte[] HexStringToBinary(String hexString){  
        //hexString的长度对2取整，作为bytes的长度  
        int len = hexString.length()/2;  
        byte[] bytes = new byte[len];  
        byte high = 0;//字节高四位  
        byte low = 0;//字节低四位  
  
        for(int i=0;i<len;i++){  
             //右移四位得到高位  
             high = (byte)((hexStr.indexOf(hexString.charAt(2*i)))<<4);  
             low = (byte)hexStr.indexOf(hexString.charAt(2*i+1));  
             bytes[i] = (byte) (high|low);//高地位做或运算  
        }  
        return bytes;  
    }  
}
