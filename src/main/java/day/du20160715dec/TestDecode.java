package day.du20160715dec;

import java.io.ByteArrayOutputStream;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import com.sun.org.apache.xml.internal.security.utils.Base64;

public class TestDecode {
	public static void main(String[] args) {
		//第一个参数为ApiSecret
		//第二个参数为ESP回调的数据
		String encryptedB64 = "h8GPkagJ1Wi7QhkO-HxfLzZs95y9uSTYtzYvX6w5ILSx5ATgWrpYklyRHgY38OF8oBbHD0HVagWHmc2OHgROcl7E9HQY--IdayU33wqSZt8AMaDJh2hZpCkdIKKqHPLpKl36DQ71gbZA2tFtMVmxrZEXfB1SIswFQIZFUV04NnYTFRExvxZ773ikGM6H09FOkGc2UzN-SNDzJCinxHxfH_gvf-qYaQ5LWs7EcFXVxEM"; 
		decryptNotifyData("0d51a6fca86d245ef539033074bc1ad5",encryptedB64);
	}
	public static void decryptNotifyData(String secret, String encryptedB64) {
		//encNotifyData
		encryptedB64 = encryptedB64.replaceAll("_", "/").replaceAll("-", "+");
		while (encryptedB64.length() % 4 != 0) {
			encryptedB64 += "=";
		}
		byte[] sec = decode(secret);
		SecretKeySpec aesKey = new SecretKeySpec(sec, 0, sec.length, "AES");

		try {
			Cipher cipher = Cipher.getInstance(aesKey.getAlgorithm());
			cipher.init(Cipher.DECRYPT_MODE, aesKey);
			byte[] decrypted = cipher.doFinal(Base64.decode(encryptedB64));
			System.out.println(new String(decrypted));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public static byte[] decode(String encoded) {
        char[] hex = encoded.toCharArray();
        if ((hex.length & 0x01) != 0) return null;
        ByteArrayOutputStream output = new ByteArrayOutputStream(hex.length >> 1);
        for (int i = 0; i < hex.length; ) {
            output.write(((digit(hex[i++]) << 4) | digit(hex[i++])) & 0xFF);
        }
        return output.toByteArray();
    }
	private static int digit(char ch) {
        return Character.digit(ch, 16);
    }
}
