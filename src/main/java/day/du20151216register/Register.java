package day.du20151216register;
import java.util.Scanner;

public class Register {

	public static void main(String[] args) {
		Register.register();
	}

	public static void register() {

		String[] regArr = new String[2];
		Scanner sc = new Scanner(System.in);

		try {

			System.out.println("请输入注册名称：");
			regArr[0] = sc.nextLine();

			System.out.println("请输入注册密码：");
			regArr[1] = sc.nextLine();

			System.out.println("注册成功");

			login(regArr); // 登陸

		} finally {
			sc.close();
		}

	}

	public static void login(String[] regArr) {

		boolean isLogin = false;
		String[] loginArr = new String[2];
		Scanner sc = new Scanner(System.in);

		try {

			System.out.println("请输入登陆名称：");
			loginArr[0] = sc.nextLine();

			System.out.println("请输入登陆密码：");
			loginArr[1] = sc.nextLine();

			isLogin = new Login().login(regArr, loginArr[0], loginArr[1]);

			if (isLogin) {
				System.out.println("登陆成功，");
				new Number().generalNumber();
			} else {
				System.out.println("登陆失败，继续登陆请按1，退出请按0");
				String info = sc.nextLine();
				if ("1".equals(info)) {
					login(regArr);
				} else if ("0".equals(info)) {
					System.out.println("谢谢使用，再见");
					System.exit(0);
				}
			}
		} finally {
			sc.close();
		}
	}

}

class Login {

	private String userName;
	private String pwd;

	public boolean login(String[] userInfo, String userName, String pwd) {
		if (userName.equals(userInfo[0]) && pwd.equals(userInfo[1])) {
			return true;
		}

		return false;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

}

class Number {

	public void generalNumber() {
		int number = 0;
		for (int i = 1; i < 1000; i++) {
			number = (int) (Math.random() * 100 + 1);
			System.out.print(number + ",");
		}
	}

}
