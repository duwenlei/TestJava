package day.du20160523log4j;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;

@SuppressWarnings("unused")
public class Log4jTest {
	
	private static Logger log = Logger.getLogger(Log4jTest.class);
	
	
	public static void main(String[] args) {
		PatternLayout layout = new PatternLayout();
        layout.setConversionPattern("%d{yyyy-MM-dd HH:mm:ss} [%p] %c{2} %m%n");
        
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
        String dateFormat = sf.format(new Date());
        
        RollingFileAppender rollingFileAppender = new RollingFileAppender();
        rollingFileAppender.setName("rollingFileAppender");
        rollingFileAppender.setMaxBackupIndex(14);
        rollingFileAppender.setMaxFileSize("100KB");
        //rollingFileAppender.setFile(dateFormat+".log");
        rollingFileAppender.setLayout(layout);
        rollingFileAppender.setThreshold(Level.DEBUG);
        rollingFileAppender.activateOptions();
        
        log.addAppender(rollingFileAppender);
        
        ConsoleAppender console = new ConsoleAppender(); //create appender
        //configure the appender
        console.setLayout(layout); 
        console.setThreshold(Level.DEBUG);
        console.activateOptions();
        
        log.addAppender(console);
        
        
        log.debug("aaaaaaaaaaaaaaaaaaaaaaaa");
        log.error("bbbbbbbbbbbbbbbbbbbbbbbb");
        log.trace("cccccccccccccccccccccccc");
        
        new Thread(
        		new Runnable() {
					public void run() {
						for (int i = 0; i < 100000; i++) {
							log.debug("aaaaaaaaaaaaaaaaaaaaaaaa : "+i);
					        log.error("bbbbbbbbbbbbbbbbbbbbbbbb : "+i);
					        log.trace("cccccccccccccccccccccccc : "+i);
						}
					}
		}).start();
	}

	private static void dailyDayFileAppender() {
		PatternLayout layout = new PatternLayout();
        layout.setConversionPattern("%d{yyyy-MM-dd HH:mm:ss} [%p] %c{2} %m%n");

        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
        String dateFormat = sf.format(new Date());

        DailyRollingFileAppender dailyRollingFileAppender = new DailyRollingFileAppender();
        dailyRollingFileAppender.setName("dailyRollingFileAppender");
        dailyRollingFileAppender.setFile(dateFormat+".log");
        dailyRollingFileAppender.setLayout(layout);
        dailyRollingFileAppender.setDatePattern("yyyy-MM-dd");
        dailyRollingFileAppender.setThreshold(Level.DEBUG);
        dailyRollingFileAppender.activateOptions();

		log.addAppender(dailyRollingFileAppender);
		
		log.debug("assssssssssssssssssssss");
	}

}
