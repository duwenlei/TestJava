package day.du20180723Mumbling;


import java.util.regex.Pattern;

public class Mumbling {

    private static Pattern pattern = Pattern.compile("\\d{4}|\\d{6}");
    public static boolean validatePin(String pin) {
        return pattern.matcher(pin).matches();
    }

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        System.out.println(Mumbling.validatePin("1234"));
        System.out.println(Mumbling.validatePin("0000"));
        System.out.println(Mumbling.validatePin("1111"));
        System.out.println(Mumbling.validatePin("123456"));
        System.out.println(Mumbling.validatePin("098765"));
        System.out.println(Mumbling.validatePin("000000"));
        System.out.println(Mumbling.validatePin("090909"));

        System.out.println(Mumbling.validatePin("1"));
        System.out.println(Mumbling.validatePin("12"));
        System.out.println(Mumbling.validatePin("123"));
        System.out.println(Mumbling.validatePin("12345"));
        System.out.println(Mumbling.validatePin("1234567"));
        System.out.println(Mumbling.validatePin("-1234"));
        System.out.println(Mumbling.validatePin("1.234"));
        System.out.println(Mumbling.validatePin("00000000"));

        System.out.println("time:"+(System.currentTimeMillis()-startTime)+"ms");
    }

}
