package day.du20180529tsa;

import org.bouncycastle.util.encoders.Base64;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Test {
    public static byte[] getTSAResponse(String tsaUrl, String tsaUser, String tsaPass, byte[] requestBytes) throws IOException {
        // Setup the TSA connection
        URL url = new URL(tsaUrl);
        URLConnection tsaConnection;
        try {
            tsaConnection = url.openConnection();
        } catch (IOException ioe) {
            throw ioe;
        }
        tsaConnection.setDoInput(true);
        tsaConnection.setDoOutput(true);
        tsaConnection.setUseCaches(false);
        //TODO: BEGIN CHANGE
        int timeout = 30 * 1000;
        tsaConnection.setConnectTimeout(timeout);
        tsaConnection.setReadTimeout(timeout);
        //END CHANGE

        tsaConnection.setRequestProperty("Content-Type", "application/timestamp-query");
        //tsaConnection.setRequestProperty("Content-Transfer-Encoding", "base64");
        tsaConnection.setRequestProperty("Content-Transfer-Encoding", "binary");

        if ((tsaUser != null) && !tsaUser.equals("")) {
            String userPassword = tsaUser + ":" + tsaPass;
            tsaConnection.setRequestProperty("Authorization", "Basic " +
                    new String(Base64.encode(userPassword.getBytes())));
        }
        OutputStream out = tsaConnection.getOutputStream();
        out.write(requestBytes);
        out.close();

        // Get TSA response as a byte array
        InputStream inp = tsaConnection.getInputStream();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int bytesRead = 0;
        while ((bytesRead = inp.read(buffer, 0, buffer.length)) >= 0) {
            baos.write(buffer, 0, bytesRead);
        }
        byte[] respBytes = baos.toByteArray();

        String encoding = tsaConnection.getContentEncoding();
        if (encoding != null && encoding.equalsIgnoreCase("base64")) {
            respBytes = Base64.decode(respBytes);
        }
        return respBytes;
    }

    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        md.update("dwl".getBytes("utf-8"));
        byte[] dataSha1 = md.digest();
        System.out.println(new String(dataSha1));
        byte[] respBytes = getTSAResponse("http://tsa.itrusign.cn/tsa/timestamp",null,null,dataSha1);
        System.out.println(new String(respBytes));
    }
}
