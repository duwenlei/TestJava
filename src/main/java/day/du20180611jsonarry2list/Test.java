package day.du20180611jsonarry2list;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

public class Test {
    public static void main(String[] args) {
        JSONObject jsonObject = new JSONObject("{signatures:[{\"positionY\":715,\"page\":1,\"userId\":\"f163be7e-9954-4415-a313-07fbd8485ad5\",\"positionX\":6},{\"positionY\":586,\"page\":1,\"userId\":\"4b29801d-bfdd-49e9-9ff0-e1d69d966f15\",\"positionX\":6}]}");
        JSONArray array = jsonObject.getJSONArray("signatures");
        List list = array.toList();
        for(Object o : list){
            System.out.println(o.toString());
        }
        System.out.println(jsonObject.toString());
    }
}
