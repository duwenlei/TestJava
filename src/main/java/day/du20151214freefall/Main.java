package day.du20151214freefall;

public class Main {

	public static void main(String[] args) {
		double highSum = 100;
		double high = 100;
		for(int i =1 ; i < 11 ; i++){
			high = getHigh(high);
			System.out.println("第"+i+"下落后,共经过了"+highSum+"M,反弹了"+high+"M");
			
			highSum += high;
		}	     
	}
	
	private static double getHigh(double high){
		return high/2;
	}
	
}
